﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading;
using System.IO.Ports;
using DataLogger.Entities;
using System.Globalization;
using DataLogger.Data;
using System.Diagnostics;
using System.IO;
using Excel = 
    //ClosedXML.Excel;
Microsoft.Office.Interop.Excel;
using System.Reflection;
using DataLogger.Utils;
using System.Resources;
using System.Net.Sockets;
using System.Net;
using WinformProtocol;
using Npgsql;
using System.Text.RegularExpressions;

namespace DataLogger
{
    public partial class frmNewMain : Form
    {
        LanguageService lang = new LanguageService(typeof(frmNewMain).Assembly);
        public static string language_code = "en";

        public bool is_close_form = false;

        public static TcpListener tcpListener = null;
        public static DateTime datetime00;

        private System.Timers.Timer tmrThreadingTimerForFTP;
        private System.Timers.Timer tmrThreadingTimerForFTP_Auto;
        private System.Threading.Timer tmrThreadingTimerForUpdateUI;

        public const int TRANSACTION_ADD_NEW = 1;
        public const int TRANSACTION_UPDATE = 2;

        // global dataValue
        int countingRequest = 0;
        public int firstTimeForIOControl = 0;

        public static measured_data objMeasuredDataGlobal = new measured_data();

        data_value obj5MinuteDataValue = new data_value();
        data_value obj60MinuteDataValue = new data_value();

        // delegate used for Invoke
        internal delegate void StringDelegate(string data);
        internal delegate void HeadingTimerDelegate(string data);
        private delegate void ProcessDataCallback(string text);
        internal delegate void SetHeadingLoginNameDelegate(string data);


        private readonly data_5minute_value_repository db5m = new data_5minute_value_repository();
        private readonly data_60minute_value_repository db60m = new data_60minute_value_repository();
        private readonly maintenance_log_repository _maintenance_logs = new maintenance_log_repository();

        public DateTime dtpDateFrom;
        public DateTime dtpDateTo;

        public static List<string> urlFolder = new List<string>();
        public static List<string> urlFile = new List<string>();
        public static Regex FtpListDirectoryDetailsRegex
        = new Regex(@".*(?<month>(Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec))\s*(?<day>[0-9]*)\s*(?<yearTime>([0-9]|:)*)\s*(?<fileName>.*)", RegexOptions.Compiled | RegexOptions.IgnoreCase);
        public static Regex FtpDirectoryNameDetailsRegex
         = new Regex(@".*(?<month>(Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec))\s*(?<day>[0-9]*)\s*(?<yearTime>([0-9]|:)*)\s*(?<fileName>)", RegexOptions.Compiled | RegexOptions.IgnoreCase);
        public static string pattern = @"^(\d+-\d+-\d+\s+\d+:\d+(?:AM|PM))\s+(<DIR>|\d+)\s+(.+)$";
        public static Form1 protocol;
        #region Form event
        private const int CP_NOCLOSE_BUTTON = 0x200;
        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams myCp = base.CreateParams;
                myCp.ClassStyle = myCp.ClassStyle | CP_NOCLOSE_BUTTON;
                return myCp;
            }
        }
        public frmNewMain()
        {
            Application.CurrentCulture = System.Globalization.CultureInfo.CreateSpecificCulture("en-US");
            InitializeComponent();
        }
        private void frmNewMain_Load(object sender, EventArgs e)
        {

            GlobalVar.maintenanceLog = new maintenance_log();

            frmConfiguration.protocol = new Form1(this);
            Thread.Sleep(1000 * 60);
            backgroundWorkerMain.RunWorkerAsync();

            initUserInterface();
            initConfig();
            //tmrThreadingTimerForFTP = new System.Threading.Timer(new TimerCallback(tmrThreadingTimerForFTP_TimerCallback), null, 1000 * 60, Timeout.Infinite);
            //tmrThreadingTimerForFTP.Change(0, 1000 * 60 * 60 * 2);
            //tmrThreadingTimerForFTP.Change(0, 1000 * 60 * 5);
            //

            tmrThreadingTimerForFTP_Auto = new System.Timers.Timer();
            tmrThreadingTimerForFTP_Auto.Interval = 1000 * 60 * 1;
            tmrThreadingTimerForFTP_Auto.Elapsed += tmrThreadingTimerForFTP_Auto_TimerCallback;
            tmrThreadingTimerForFTP_Auto.AutoReset = false;
            tmrThreadingTimerForFTP_Auto.Start();

            tmrThreadingTimerForFTP = new System.Timers.Timer();
            tmrThreadingTimerForFTP.Interval = 1000 * 60 * 1;
            tmrThreadingTimerForFTP.Elapsed += tmrThreadingTimerForFTP_TimerCallback;
            tmrThreadingTimerForFTP.AutoReset = false;
            tmrThreadingTimerForFTP.Start();
            //
            tmrThreadingTimerForUpdateUI = new System.Threading.Timer(new TimerCallback(tmrThreadingTimerForUpdateUI_TimerCallback), null, 1000 * 60 * 5, Timeout.Infinite);
            tmrThreadingTimerForUpdateUI.Change(1000, 1000 * 60 * 1);



        }
        private void initConfig()
        {
            GlobalVar.stationSettings = new station_repository().get_info();
            GlobalVar.moduleSettings = new module_repository().get_all();

            label9.Text = Convert.ToString(GlobalVar.stationSettings.station_name);

            for (int i = 1; i <= GlobalVar.moduleSettings.Count(); i++)
            {
                foreach (var item in GlobalVar.moduleSettings)
                {
                    string currentvar = "var" + i.ToString();
                    string currentlabelname = "txt" + currentvar;
                    string currentlabelunit = "txt" + currentvar + "Unit";
                    string currentlabelvalue = "txt" + currentvar + "Value";
                    if (item.item_name.Equals(currentvar))
                    {

                        ClearLabel(this, item.display_name, currentlabelname);
                        ClearLabel(this, item.unit, currentlabelunit);
                        try
                        {
                            ClearTextbox(this, "-", currentlabelvalue);
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine(e.StackTrace);
                        }
                    }
                }
            }
        }
        private void btnExit_Click(object sender, EventArgs e)
        {
            try
            {
                tcpListener.Stop();
            }
            catch (Exception ex)
            {
            }
            this.Close();
        }
        #endregion
        private void backgroundWorkerMain_DoWork(object sender, DoWorkEventArgs e)
        {
            station existedStationsSetting = new station_repository().get_info();
            if (existedStationsSetting == null)
            {

            }
            else
            {


            }
        }
        #region initial method
        private void initUserInterface()
        {

            switch_language();
        }
        private void switch_language()
        {
            lang.nextLanguage();
            switch (lang.CurrentLanguage.Language)
            {
                case ELanguage.English:
                    language_code = "en";
                    this.btnMonthlyReport.BackgroundImage = global::DataLogger.Properties.Resources.MonthlyReportButton;
                    break;
                case ELanguage.Vietnamese:
                    language_code = "vi";
                    this.btnMonthlyReport.BackgroundImage = global::DataLogger.Properties.Resources.MonthlyReportButton;
                    break;
                default:
                    break;
            }
            this.btnLanguage.BackgroundImage = lang.CurrentLanguage.Icon;
            // heading menu
            lang.setText(lblHeaderNationName, "main_menu_language");
            lang.setText(lblMainMenuTitle, "main_menu_title");
            settingForLoginStatus();
            // left menu buttong
            lang.setText(lblThaiNguyenStation, "thai_nguyen_station_text", EAlign.Center);
            lang.setText(lblAutomaticMonitoring, "automatic_monitoring_text", EAlign.Center);
            lang.setText(lblSurfaceWaterQuality, "surface_water_quality_text", EAlign.Center);
            // control panel
            lang.setText(this, "data_logger_system");
        }
        #endregion

        #region ComPort Process data
        private void setText(string text)
        {
            if (this.txtData.InvokeRequired)
            {
                StringDelegate d = new StringDelegate(setText);
                this.txtData.Invoke(d, new object[] { text });
            }
            else
            {
                txtData.Text = text;
            }
        }
        private void setTextHeadingTimer(string text)
        {
            if (this.txtData.InvokeRequired)
            {
                HeadingTimerDelegate d = new HeadingTimerDelegate(setTextHeadingTimer);
                this.lblHeadingTime.Invoke(d, new object[] { text });
            }
            else
            {
                lblHeadingTime.Text = text;
            }
        }
        private void setTextHeadingLogin(string text)
        {
            if (this.txtData.InvokeRequired)
            {
                SetHeadingLoginNameDelegate d = new SetHeadingLoginNameDelegate(setTextHeadingLogin);
                this.lblLoginDisplayName.Invoke(d, new object[] { text });
            }
            else
            {
                lblLoginDisplayName.Text = text;
            }
        }
        public static ASCIIEncoding _encoder = new ASCIIEncoding();
        public void writeLog(string content, string filename)
        {
            try
            {
                if (!File.Exists(filename))
                {
                    File.Create(filename);
                }

                TextWriter twr = new StreamWriter(filename, true);
                DateTime dt = new DateTime();
                dt = DateTime.Now;
                twr.Write(dt.ToString() + " : ");
                twr.WriteLine(content);
                twr.Close();
            }
            catch (Exception ex)
            {
                System.Console.WriteLine("Error: -" + ex.Message);
            }
        }
        public static string StringToByteArray(string hexstring)
        {
            return String.Join(String.Empty, hexstring
                .Select(c => Convert.ToString(Convert.ToInt32(c.ToString(), 16), 2).PadLeft(4, '0')));
        }
        #endregion
        #region threading timer
        public int indexSelection = 0;
        public int indexSelectionStation = 0;
        private void tmrThreadingTimerForUpdateUI_TimerCallback(object state)
        {
            try
            {
                data_value obj = new data_5minute_value_repository().get_latest_info();
                GlobalVar.moduleSettings = new module_repository().get_all();
                foreach (var item in GlobalVar.moduleSettings)
                {
                    if (!item.display_name.Equals("---"))
                    {
                        string currentvar = item.item_name;
                        string currentlabel = "txt" + currentvar;
                        string currentlabelvalue = "txt" + currentvar + "Value";

                        string param = currentvar;  //VD : var1
                        string param_status = currentvar + "_status";

                        Type paramType = typeof(data_value);
                        PropertyInfo prop = paramType.GetProperty(param);
                        PropertyInfo prop_status = paramType.GetProperty(param_status);
                        var status = prop_status.GetValue(obj);

                        Type paramTypeTextBox = typeof(TextBox);
                        PropertyInfo propTextBox = paramType.GetProperty(param);
                        string value = "--";
                        GetTextBoxStrings(this, currentlabelvalue, ref value);

                        if ((int)status != 0)
                        {

                        }
                        else
                        {
                            value = ((double)prop.GetValue(obj)).ToString("##0.00"); //VD : lay display_var = data.var1
                            module objvar = item;
                            if (Double.Parse(value) > objvar.error_max || Double.Parse(value) < objvar.error_min)
                            {
                                SetColorLabel(this, currentlabel, System.Drawing.Color.Red);
                                SetColorTextbox(this, currentlabelvalue, System.Drawing.Color.Red);
                                //txtvar1Value.ForeColor = Color.Red;
                                //txtvar1.ForeColor = Color.Red;
                            }
                            else
                            {
                                SetColorLabel(this, currentlabel, System.Drawing.Color.Black);
                                SetColorTextbox(this, currentlabelvalue, Color.FromArgb(45, 160, 186));
                            }
                        }

                        if (item.item_name.Equals(currentvar))
                        {
                            SetValueTextbox(this, value, currentlabelvalue);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.StackTrace);
            }
        }
        private void tmrThreadingTimerForFTP_TimerCallback(Object myObject, EventArgs myEventArgs)
        {
            try
            {
                tmrThreadingTimerForFTP.Stop();
                GlobalVar.stationSettings = new station_repository().get_info();
                int flag = GlobalVar.stationSettings.ftpflag;
                Console.WriteLine("MANUAL :" + "Loop " + DateTime.Now);
                push_server_repository s = new push_server_repository();
                List<push_server> listUser = s.get_all();

                string appPath = Path.GetDirectoryName(Application.ExecutablePath);
                string localFTP = "localFTP";
                string localFTPPath = Path.Combine(appPath, localFTP);
                if (!Directory.Exists(localFTPPath))
                {
                    // Try to create the directory.
                    DirectoryInfo di = Directory.CreateDirectory(localFTPPath);
                }
                if (true)
                {
                    if (GlobalVar.stationSettings.ftpflag >= 1)
                    {
                        if (Application.OpenForms.OfType<Form1>().Count() == 1)
                        {
                        }
                        foreach (push_server push_server in listUser)
                        {
                            if (push_server.ftp_flag >= 1)
                            {
                                //List<string> listFile = DownloadremoteFTP(push_server, localFTPPath);
                                List<string> listFile = GetLocalFTP(push_server, localFTPPath);
                                LoadlocalFTP(localFTPPath, push_server, listFile);
                            }
                        }
                    }
                }
                Console.WriteLine("MANUAL:" + "END1");
                Console.WriteLine("MANUAL :" + "Start New Loop 1 " + DateTime.Now);
                tmrThreadingTimerForFTP.Interval = 1000 * 60 * 15;
                tmrThreadingTimerForFTP.Start();
            }
            catch (Exception e)
            {
                Console.WriteLine("MANUAL :" + e.StackTrace);
                Console.WriteLine("MANUAL :" + e.Message);
                tmrThreadingTimerForFTP.Stop();
                Console.WriteLine("MANUAL:" + "END2");
                Console.WriteLine("MANUAL :" + "Start New Loop 2");
                tmrThreadingTimerForFTP.Interval = 1000 * 60 * 15;
                tmrThreadingTimerForFTP.Start();
            }
        }
        private void tmrThreadingTimerForFTP_Auto_TimerCallback(Object myObject, EventArgs myEventArgs)
        {
            try
            {
                tmrThreadingTimerForFTP_Auto.Stop();
                GlobalVar.stationSettings = new station_repository().get_info();
                int flag = GlobalVar.stationSettings.ftpflag;
                Console.WriteLine("AUTO:" + "Loop " + DateTime.Now);
                push_server_repository s = new push_server_repository();
                List<push_server> listUser = s.get_all();

                string appPath = Path.GetDirectoryName(Application.ExecutablePath);
                string localFTP = "localFTP";
                string localFTPPath = Path.Combine(appPath, localFTP);
                if (!Directory.Exists(localFTPPath))
                {
                    // Try to create the directory.
                    DirectoryInfo di = Directory.CreateDirectory(localFTPPath);
                }
                if (true)
                {
                    if (GlobalVar.stationSettings.ftpflag >= 1)
                    {
                        if (Application.OpenForms.OfType<Form1>().Count() == 1)
                        {
                        }
                        foreach (push_server push_server in listUser)
                        {
                            if (push_server.ftp_flag >= 1)
                            {
                                //List<string> listFile = DownloadremoteFTP_Auto(push_server, localFTPPath);
                                //List<string> listFile = DownloadremoteFTP(push_server, localFTPPath);
                                List<string> listFile = GetLocalFTP_Auto(push_server, localFTPPath);

                                LoadlocalFTP_Auto(localFTPPath, push_server,listFile);
                            }
                        }
                    }
                }
                Console.WriteLine("AUTO:" + "END1" );
                Console.WriteLine("AUTO:" + "Start New Loop 1 " + DateTime.Now);
                tmrThreadingTimerForFTP_Auto.Interval = 1000 * 60;
                tmrThreadingTimerForFTP_Auto.Start();
            }
            catch (Exception e)
            {
                Console.WriteLine("AUTO:" + e.StackTrace);
                Console.WriteLine("AUTO:" + e.Message);
                tmrThreadingTimerForFTP_Auto.Stop();
                Console.WriteLine("AUTO:" + "END2");
                Console.WriteLine("AUTO:" + "Start New Loop 2");
                tmrThreadingTimerForFTP_Auto.Interval = 1000 * 60;
                tmrThreadingTimerForFTP_Auto.Start();
            }
        }
        #endregion

        #region Utility
        public string HEX_Coding(string aHex)
        {
            switch (aHex)
            {
                case "A":
                    return ":";

                case "B":
                    return ";";

                case "C":
                    return "<";

                case "D":
                    return "=";

                case "E":
                    return ">";

                case "F":
                    return "?";
            }
            return aHex;
        }
        private string Checksum(byte[] ByteArray)
        {
            int num = 0;
            int num2 = ByteArray.Length - 1;
            for (int i = 0; i <= num2; i++)
            {
                num += ByteArray[i];
            }
            num = num % 0x100;
            return (this.HEX_Coding(((int)(num / 0x10)).ToString("X")) + this.HEX_Coding(((int)(num % 0x10)).ToString("X")));
        }
        private void GetTextBoxStrings(System.Windows.Forms.Control control, string textboxname,ref string text)
        {
            if (control is TextBox)
            {
                TextBox tb = (TextBox)control;
                if (tb.Name.Equals(textboxname))
                {
                    text = tb.Text;
                }

            }
            else
            {
                foreach (System.Windows.Forms.Control child in control.Controls)
                {
                    GetTextBoxStrings(child, textboxname, ref text);
                }
                //return null;
            }
            //return null;
        }
        public static void SetColorLabel(System.Windows.Forms.Control control, string label, Color color)
        {
            if (control is Label)
            {
                Label tb = (Label)control;
                if (tb.Name.Equals(label))
                {
                    if (tb.InvokeRequired)
                    {
                        tb.Invoke(new MethodInvoker(delegate
                        {
                            //tb.Text = text;
                            tb.ForeColor = color;
                        }));
                    }
                    else
                    {
                        //tb.Text = text;
                        tb.ForeColor = color;
                    }
                }
            }
            else
            {
                foreach (System.Windows.Forms.Control child in control.Controls)
                {
                    SetColorLabel(child, label, color);
                }
            }
        }
        public static void SetColorTextbox(System.Windows.Forms.Control control, string label, Color color)
        {
            if (control is TextBox)
            {
                TextBox tb = (TextBox)control;
                if (tb.Name.Equals(label))
                {
                    if (tb.InvokeRequired)
                    {
                        tb.Invoke(new MethodInvoker(delegate
                        {
                            //tb.Text = text;
                            tb.ForeColor = color;
                        }));
                    }
                    else
                    {
                        //tb.Text = text;
                        tb.ForeColor = color;
                    }
                }
            }
            else
            {
                foreach (System.Windows.Forms.Control child in control.Controls)
                {
                    SetColorTextbox(child, label, color);
                }
            }
        }
        public static void SetValueTextbox(System.Windows.Forms.Control control, string text, string label)
        {
            if (control is TextBox)
            {
                TextBox tb = (TextBox)control;
                if (tb.Name.Equals(label))
                {
                    if (tb.InvokeRequired)
                    {
                        tb.Invoke(new MethodInvoker(delegate { tb.Text = text; }));
                    }
                    else
                    {
                        tb.Text = text;
                    }
                }

            }
            else
            {
                foreach (System.Windows.Forms.Control child in control.Controls)
                {
                    SetValueTextbox(child, text, label);
                }
            }

        }
        public static void ClearLabel(System.Windows.Forms.Control control, string text, string label)
        {
            if (control is Label)
            {
                Label lbl = (Label)control;
                if (lbl.Name.Equals(label))
                    lbl.Text = text;

            }
            else
            {
                foreach (System.Windows.Forms.Control child in control.Controls)
                {
                    ClearLabel(child, text, label);
                }
            }

        }
        public static void ClearTextbox(System.Windows.Forms.Control control, string text, string label)
        {
            if (control is TextBox)
            {
                TextBox tb = (TextBox)control;
                if (tb.Name.Equals(label))
                    tb.Text = text;

            }
            else
            {
                foreach (System.Windows.Forms.Control child in control.Controls)
                {
                    ClearTextbox(child, text, label);
                }
            }

        }
        private static int getMinValueFromDatabinding(string code)
        {
            try
            {
                //String connstring = "Server = localhost;Port = 5432; User Id = postgres;Password = 123;Database = DataLoggerDB";
                //NpgsqlConnection conn = new NpgsqlConnection(connstring);
                //conn.Open();
                using (NpgsqlDBConnection db = new NpgsqlDBConnection())
                {
                    if (db.open_connection())
                    {
                        using (NpgsqlCommand cmd = db._conn.CreateCommand())
                        {
                            string sql_command1 = "SELECT * from " + "databinding";
                            cmd.CommandText = sql_command1;
                            NpgsqlDataReader dr = cmd.ExecuteReader();
                            DataTable tbcode = new DataTable();
                            tbcode.Load(dr); // Load bang chua mapping cac truong
                            int min_value = -1;
                            foreach (DataRow row2 in tbcode.Rows)
                            {
                                if (Convert.ToString(row2["code"]).Equals(code))
                                {
                                    min_value = Convert.ToInt32(row2["min_value"]);
                                    break;
                                }
                            }
                            db.close_connection();
                            return min_value;
                        }
                    }
                    else
                    {
                        db.close_connection();
                        return -1;
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.StackTrace);
                Console.WriteLine(e.Message);
                return -1;
            }
        }
        private static string ByteArrayToHexString(byte[] data)
        {
            StringBuilder sb = new StringBuilder(data.Length * 3);
            try
            {
                foreach (byte b in data)
                    sb.Append(Convert.ToString(b, 16).PadLeft(2, '0') + "");
            }
            catch (Exception)
            {
                return "Error";
            }
            return sb.ToString().ToUpper();
        }
        private static Single ConvertHexToSingle(string hexVal)
        {
            try
            {
                int i = 0, j = 0;
                byte[] bArray = new byte[4];


                for (i = 0; i <= hexVal.Length - 1; i += 2)
                {
                    bArray[j] = Byte.Parse(hexVal[i].ToString() + hexVal[i + 1].ToString(), System.Globalization.NumberStyles.HexNumber);
                    j += 1;
                }
                Array.Reverse(bArray);
                Single s = BitConverter.ToSingle(bArray, 0);
                return (s);
            }
            catch (Exception ex)
            {
                throw new FormatException("The supplied hex value is either empty or in an incorrect format. Use the " +
                "following format: 00000000", ex);
            }
        }
        public static byte[] SubArray(byte[] data, int index, int length)
        {
            byte[] result = new byte[length];
            Array.Copy(data, index, result, 0, length);
            return result;
        }
        public static string ByteArrayToString(byte[] ba)
        {
            StringBuilder hex = new StringBuilder(ba.Length * 2);
            foreach (byte b in ba)
                hex.AppendFormat("{0:x2}", b);
            return hex.ToString();
        }
        private static byte[] Combine(byte[] first, int first_length, byte[] second)
        {
            byte[] ret = new byte[first_length + second.Length];
            try
            {
                Buffer.BlockCopy(first, 0, ret, 0, first_length);
                Buffer.BlockCopy(second, 0, ret, first_length, second.Length);
            }
            catch (Exception ex)
            {
                //MessageBox.Show("0003," + ex.Message);
            }


            return ret;
        }
        public Boolean iSAllMinValue(data_value data)
        {
            using (NpgsqlDBConnection db = new NpgsqlDBConnection())
            {
                try
                {
                    if (db.open_connection())
                    {
                        string sql_command1 = "SELECT * from " + "databinding";
                        using (NpgsqlCommand cmd = db._conn.CreateCommand())
                        {
                            cmd.CommandText = sql_command1;
                            NpgsqlDataReader dr;
                            dr = cmd.ExecuteReader();
                            DataTable tbcode = new DataTable();
                            tbcode.Load(dr); // Load bang chua mapping cac truong
                            int countNull = 0;
                            foreach (DataRow row2 in tbcode.Rows)
                            {
                                string code = Convert.ToString(row2["code"]);
                                int min_value = Convert.ToInt32(row2["min_value"]);
                                switch (code)
                                {
                                    case "var1":
                                        if (Convert.ToDouble(String.Format("{0:0.00}", data.var1)) >= min_value && Convert.ToDouble(String.Format("{0:0.00}", data.var1)) != -1)
                                        {
                                        }
                                        else
                                        {
                                            countNull++;
                                        }
                                        break;
                                    case "var2":
                                        if (Convert.ToDouble(String.Format("{0:0.00}", data.var2)) >= min_value && Convert.ToDouble(String.Format("{0:0.00}", data.var2)) != -1)
                                        {
                                        }
                                        else
                                        {
                                            countNull++;
                                        }
                                        break;
                                    case "var3":
                                        if (Convert.ToDouble(String.Format("{0:0.00}", data.var3)) >= min_value && Convert.ToDouble(String.Format("{0:0.00}", data.var3)) != -1)
                                        {
                                        }
                                        else
                                        {
                                            countNull++;
                                        }
                                        break;
                                    case "var4":
                                        if (Convert.ToDouble(String.Format("{0:0.00}", data.var4)) >= min_value && Convert.ToDouble(String.Format("{0:0.00}", data.var4)) != -1)
                                        {
                                        }
                                        else
                                        {
                                            countNull++;
                                        }
                                        break;
                                    case "var5":
                                        if (Convert.ToDouble(String.Format("{0:0.00}", data.var5)) >= min_value && Convert.ToDouble(String.Format("{0:0.00}", data.var5)) != -1)
                                        {
                                        }
                                        else
                                        {
                                            countNull++;
                                        }
                                        break;
                                    case "var6":
                                        if (Convert.ToDouble(String.Format("{0:0.00}", data.var6)) >= min_value && Convert.ToDouble(String.Format("{0:0.00}", data.var6)) != -1)
                                        {
                                        }
                                        else
                                        {
                                            countNull++;
                                        }
                                        break;
                                    case "var7":
                                        if (Convert.ToDouble(String.Format("{0:0.00}", data.var7)) >= min_value && Convert.ToDouble(String.Format("{0:0.00}", data.var7)) != -1)
                                        {
                                        }
                                        else
                                        {
                                            countNull++;
                                        }
                                        break;
                                    case "var8":
                                        if (Convert.ToDouble(String.Format("{0:0.00}", data.var8)) >= min_value && Convert.ToDouble(String.Format("{0:0.00}", data.var8)) != -1)
                                        {
                                        }
                                        else
                                        {
                                            countNull++;
                                        }
                                        break;
                                    case "var9":
                                        if (Convert.ToDouble(String.Format("{0:0.00}", data.var9)) >= min_value && Convert.ToDouble(String.Format("{0:0.00}", data.var9)) != -1)
                                        {
                                        }
                                        else
                                        {
                                            countNull++;
                                        }
                                        break;
                                    case "var10":
                                        if (Convert.ToDouble(String.Format("{0:0.00}", data.var10)) >= min_value && Convert.ToDouble(String.Format("{0:0.00}", data.var10)) != -1)
                                        {
                                        }
                                        else
                                        {
                                            countNull++;
                                        }
                                        break;
                                    case "var11":
                                        if (Convert.ToDouble(String.Format("{0:0.00}", data.var11)) >= min_value && Convert.ToDouble(String.Format("{0:0.00}", data.var11)) != -1)
                                        {
                                        }
                                        else
                                        {
                                            countNull++;
                                        }
                                        break;
                                    case "var12":
                                        if (Convert.ToDouble(String.Format("{0:0.00}", data.var12)) >= min_value && Convert.ToDouble(String.Format("{0:0.00}", data.var12)) != -1)
                                        {
                                        }
                                        else
                                        {
                                            countNull++;
                                        }
                                        break;
                                    case "var13":
                                        if (Convert.ToDouble(String.Format("{0:0.00}", data.var13)) >= min_value && Convert.ToDouble(String.Format("{0:0.00}", data.var13)) != -1)
                                        {
                                        }
                                        else
                                        {
                                            countNull++;
                                        }
                                        break;
                                    case "var14":
                                        if (Convert.ToDouble(String.Format("{0:0.00}", data.var14)) >= min_value && Convert.ToDouble(String.Format("{0:0.00}", data.var14)) != -1)
                                        {
                                        }
                                        else
                                        {
                                            countNull++;
                                        }
                                        break;
                                    case "var15":
                                        if (Convert.ToDouble(String.Format("{0:0.00}", data.var15)) >= min_value && Convert.ToDouble(String.Format("{0:0.00}", data.var15)) != -1)
                                        {
                                        }
                                        else
                                        {
                                            countNull++;
                                        }
                                        break;
                                    case "var16":
                                        if (Convert.ToDouble(String.Format("{0:0.00}", data.var16)) >= min_value && Convert.ToDouble(String.Format("{0:0.00}", data.var16)) != -1)
                                        {
                                        }
                                        else
                                        {
                                            countNull++;
                                        }
                                        break;
                                    case "var17":
                                        if (Convert.ToDouble(String.Format("{0:0.00}", data.var17)) >= min_value && Convert.ToDouble(String.Format("{0:0.00}", data.var17)) != -1)
                                        {
                                        }
                                        else
                                        {
                                            countNull++;
                                        }
                                        break;
                                    case "var18":
                                        if (Convert.ToDouble(String.Format("{0:0.00}", data.var18)) >= min_value && Convert.ToDouble(String.Format("{0:0.00}", data.var18)) != -1)
                                        {
                                        }
                                        else
                                        {
                                            countNull++;
                                        }
                                        break;
                                }
                            }
                            if (countNull >= tbcode.Rows.Count)
                            {
                                return false;
                            }
                            else
                            {
                                return true;
                            }
                            db.close_connection();
                        }
                    }
                    else
                    {
                        db.close_connection();
                        return false;
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.StackTrace);
                    return false;
                }
            }
        }
        string[] a = null;
        string b = null;
        public void LoadlocalFTP(string folder, push_server push_server, List<string> listFile)
        {
            //ftp ftpClient = new ftp(server, createdFile, pwd);
            try
            {
                obj5MinuteDataValue = new data_value();
                string folderPath = folder;
                //string[] filePaths = Directory.GetFiles(folderPath, "*.txt", SearchOption.AllDirectories);
                station existedStationsSetting = new station_repository().get_info();
                int existedStationsSetting_flag = push_server.ftp_flag;
                data_value datavalue = obj5MinuteDataValue;
                DateTime created;
                string datetime = null;
                foreach (string textPath in listFile)
                {
                    datavalue = new data_value();
                    //DateTime createdFile = File.GetCreationTime(textPath);
                    string formatString = "yyyyMMddHHmmss";
                    //string sample = "20100611221912";
                    DateTime createdFile = DateTime.ParseExact(textPath.Substring(textPath.Length - 18, 14), formatString, null);
                    int result = DateTime.Compare(createdFile, push_server.ftp_lasted);
                    if (
                        result >= 0
                        //result < 0
                        //true
                        )
                    {
                        string[] lines = System.IO.File.ReadAllLines(textPath);
                        string stationID = existedStationsSetting.station_id;

                        string[] separators1 = { "_" };
                        a = lines;
                        b = textPath;
                        string[] lines0 = null;

                        //lines0 = lines[0].Split(separators1, StringSplitOptions.RemoveEmptyEntries);

                        string[] separators3 = { "_" };

                        if (
                            //lines0[0].Equals(stationID)
                            true
                            )
                        {
                            List<string> custom_param_list = new List<string>();
                            if (existedStationsSetting_flag == 11)
                            {
                                lines = lines.Skip(1).ToArray();
                            }
                            else if (existedStationsSetting_flag == 12)
                            {

                            }
                            int flag = 0;
                            foreach (string line in lines)
                            {
                                string[] separators2 = { "\t" };
                                string[] param = line.Split(separators2, StringSplitOptions.RemoveEmptyEntries);

                                string formatString2 = "yyyyMMddHHmm";
                                string dt = null;
                                if (existedStationsSetting_flag == 11)
                                {
                                    dt = param[0].Substring(0, 12);
                                }
                                else if (existedStationsSetting_flag == 12)
                                {
                                    dt = param[3].Substring(0, 12);
                                }

                                datetime = dt.Substring(0, 10) + "00";
                                datavalue.created = DateTime.ParseExact(dt, formatString2, null);
                                datavalue.stored_date = datavalue.created.Date;
                                datavalue.stored_hour = datavalue.created.Hour;
                                datavalue.stored_minute = datavalue.created.Minute;

                                GlobalVar.moduleSettings = new module_repository().get_all();

                                module resultModule = null;

                                if (existedStationsSetting_flag == 11)
                                {
                                    resultModule = GlobalVar.moduleSettings.First(a => a.code.Equals(param[1].ToLower()));
                                }
                                else if (existedStationsSetting_flag == 12)
                                {
                                    try
                                    {
                                        resultModule = GlobalVar.moduleSettings.First(a => a.code.Equals(param[0].ToLower()));
                                    }
                                    catch (InvalidOperationException e)
                                    {
                                        resultModule = null;
                                    }
                                }

                                if (resultModule != null)
                                {

                                    Type type = typeof(data_value);
                                    PropertyInfo prop = type.GetProperty(resultModule.item_name);

                                    if (existedStationsSetting_flag == 11)
                                    {
                                        double value;
                                        value = Double.Parse(param[2], CultureInfo.InvariantCulture);
                                        prop.SetValue(datavalue, value, null);
                                    }
                                    else if (existedStationsSetting_flag == 12)
                                    {
                                        if (!param[1].Equals("NULL"))
                                        {
                                            double value;
                                            value = Double.Parse(param[1], CultureInfo.InvariantCulture);
                                            prop.SetValue(datavalue, value, null);
                                        }
                                    }

                                    Type typeStatus = typeof(data_value);
                                    PropertyInfo propStatus = type.GetProperty(resultModule.item_name + "_status");
                                    propStatus.SetValue(datavalue, 0, null);

                                    custom_param_list.Add(resultModule.item_name);
                                    flag = 1;
                                }

                            }
                            data_5minute_value_repository s = new data_5minute_value_repository();
                            if (datetime == null)
                            {
                                //break;
                            }
                            else if (flag == 1)
                            {
                                string dtparam = datetime.Substring(0, 4) + "-" + datetime.Substring(4, 2) + "-" + datetime.Substring(6, 2);
                                string hourparam = datavalue.stored_hour.ToString();
                                string minuteparam = datavalue.stored_minute.ToString();
                                int id = s.get_id_by_dt(dtparam, hourparam, minuteparam);
                                datavalue.id = id;
                                if (id != -1)
                                {
                                    if (new data_5minute_value_repository().updateCustom(ref datavalue, custom_param_list) > 0)
                                    {
                                        // ok
                                    }
                                    else
                                    {
                                        // fail
                                    }

                                }
                                else
                                {
                                    if (new data_5minute_value_repository().add(ref datavalue) > 0)
                                    {
                                        // ok
                                    }
                                    else
                                    {
                                        // fail
                                    }
                                }
                            }
                        }

                        push_server_repository push_server_repository = new push_server_repository();
                        int idLasted = push_server.id;
                        push_server set = new push_server();
                        set.ftp_ip = push_server.ftp_ip;
                        set.ftp_username = push_server.ftp_username;
                        set.ftp_pwd = push_server.ftp_pwd;
                        set.ftp_folder = push_server.ftp_folder;
                        set.ftp_flag = push_server.ftp_flag;
                        set.ftp_lasted = createdFile;
                        set.ftp_lasted_manual = push_server.ftp_lasted_manual;

                        push_server_repository.update_with_id(ref set, idLasted);
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.StackTrace);
            }
        }
        public void LoadlocalFTP_Auto(string folder, push_server push_server, List<string> listFile)
        {
            try
            {
                obj5MinuteDataValue = new data_value();
                string folderPath = folder;
                //string[] filePaths = Directory.GetFiles(folderPath, "*.txt", SearchOption.AllDirectories);
                station existedStationsSetting = new station_repository().get_info();
                int existedStationsSetting_flag = push_server.ftp_flag;
                data_value datavalue = obj5MinuteDataValue;
                DateTime created;
                string datetime = null;
                foreach (string textPath in listFile)
                {
                    datavalue = new data_value();
                    //DateTime createdFile = File.GetCreationTime(textPath);
                    string formatString = "yyyyMMddHHmmss";
                    //string sample = "20100611221912";
                    DateTime createdFile = DateTime.ParseExact(textPath.Substring(textPath.Length - 18, 14), formatString, null);
                    int result = DateTime.Compare(createdFile, push_server.ftp_lasted);
                    if (
                        result >= 0
                        //result < 0
                        //true
                        )
                    {
                        string[] lines = System.IO.File.ReadAllLines(textPath);
                        string stationID = existedStationsSetting.station_id;

                        string[] separators1 = { "_" };
                        a = lines;
                        b = textPath;
                        string[] lines0 = null;

                        //lines0 = lines[0].Split(separators1, StringSplitOptions.RemoveEmptyEntries);

                        string[] separators3 = { "_" };

                        if (
                            //lines0[0].Equals(stationID)
                            true
                            )
                        {
                            List<string> custom_param_list = new List<string>();
                            if (existedStationsSetting_flag == 11)
                            {
                                lines = lines.Skip(1).ToArray();
                            }
                            else if (existedStationsSetting_flag == 12)
                            {

                            }
                            int flag = 0;
                            foreach (string line in lines)
                            {
                                string[] separators2 = { "\t" };
                                string[] param = line.Split(separators2, StringSplitOptions.RemoveEmptyEntries);

                                string formatString2 = "yyyyMMddHHmm";
                                string dt = null;
                                if (existedStationsSetting_flag == 11)
                                {
                                    dt = param[0].Substring(0, 12);
                                }
                                else if (existedStationsSetting_flag == 12)
                                {
                                    dt = param[3].Substring(0, 12);
                                }

                                datetime = dt.Substring(0, 10) + "00";
                                datavalue.created = DateTime.ParseExact(dt, formatString2, null);
                                datavalue.stored_date = datavalue.created.Date;
                                datavalue.stored_hour = datavalue.created.Hour;
                                datavalue.stored_minute = datavalue.created.Minute;

                                GlobalVar.moduleSettings = new module_repository().get_all();

                                module resultModule = null;

                                if (existedStationsSetting_flag == 11)
                                {
                                    resultModule = GlobalVar.moduleSettings.First(a => a.code.Equals(param[1].ToLower()));
                                }
                                else if (existedStationsSetting_flag == 12)
                                {
                                    try
                                    {
                                        resultModule = GlobalVar.moduleSettings.First(a => a.code.Equals(param[0].ToLower()));
                                    }
                                    catch (InvalidOperationException e)
                                    {
                                        resultModule = null;
                                    }
                                }

                                if (resultModule != null)
                                {

                                    Type type = typeof(data_value);
                                    PropertyInfo prop = type.GetProperty(resultModule.item_name);

                                    if (existedStationsSetting_flag == 11)
                                    {
                                        double value;
                                        value = Double.Parse(param[2], CultureInfo.InvariantCulture);
                                        prop.SetValue(datavalue, value, null);
                                    }
                                    else if (existedStationsSetting_flag == 12)
                                    {
                                        if (!param[1].Equals("NULL"))
                                        {
                                            double value;
                                            value = Double.Parse(param[1], CultureInfo.InvariantCulture);
                                            prop.SetValue(datavalue, value, null);
                                        }
                                    }

                                    Type typeStatus = typeof(data_value);
                                    PropertyInfo propStatus = type.GetProperty(resultModule.item_name + "_status");
                                    propStatus.SetValue(datavalue, 0, null);

                                    custom_param_list.Add(resultModule.item_name);
                                    flag = 1;
                                }

                            }
                            data_5minute_value_repository s = new data_5minute_value_repository();
                            if (datetime == null)
                            {
                                //break;
                            }
                            else if (flag == 1)
                            {
                                string dtparam = datetime.Substring(0, 4) + "-" + datetime.Substring(4, 2) + "-" + datetime.Substring(6, 2);
                                string hourparam = datavalue.stored_hour.ToString();
                                string minuteparam = datavalue.stored_minute.ToString();
                                int id = s.get_id_by_dt(dtparam, hourparam, minuteparam);
                                datavalue.id = id;
                                if (id != -1)
                                {
                                    if (new data_5minute_value_repository().updateCustom(ref datavalue, custom_param_list) > 0)
                                    {
                                        // ok
                                    }
                                    else
                                    {
                                        // fail
                                    }

                                }
                                else
                                {
                                    if (new data_5minute_value_repository().add(ref datavalue) > 0)
                                    {
                                        // ok
                                    }
                                    else
                                    {
                                        // fail
                                    }
                                }
                            }
                        }

                        //push_server_repository push_server_repository = new push_server_repository();
                        //int idLasted = push_server.id;
                        //push_server set = new push_server();
                        //set.ftp_ip = push_server.ftp_ip;
                        //set.ftp_username = push_server.ftp_username;
                        //set.ftp_pwd = push_server.ftp_pwd;
                        //set.ftp_folder = push_server.ftp_folder;
                        //set.ftp_flag = push_server.ftp_flag;
                        //set.ftp_lasted = createdFile;
                        //set.ftp_lasted_manual = push_server.ftp_lasted_manual;

                        //push_server_repository.update_with_id(ref set, idLasted);
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.StackTrace);
            }
        }
        public List<string> GetLocalFTP(push_server push_server, string localFolder) {
            string localFTPFolder = push_server.ftp_folder;
            localFolder = localFolder + "\\";
            List<string> __s = null;
            try
            {
                DirectoryInfo d = new DirectoryInfo(localFTPFolder); //Assuming Test is your Folder
                FileInfo[] Files = d.GetFiles("*.txt", SearchOption.AllDirectories); //Getting Text files

                //foreach (FileInfo file in Files)
                //{
                //    if (file.CreationTime.CompareTo(push_server.ftp_lasted) > 0) {
                //        s.Add(file.FullName);
                //    }
                //}
                var _s = Files.Select(y => new { y.FullName, y.Name, y.CreationTime })
                    .Where(x => x.CreationTime.CompareTo(push_server.ftp_lasted) > 0)
                    .ToList();

                __s = _s.Select(y => localFolder + y.Name).ToList();

                foreach (var ele in _s)
                {
                    var index = _s.IndexOf(ele);
                    System.IO.File.Copy(ele.FullName, __s.ElementAt(index), true);
                }
            }
            catch (Exception e) {

                Console.WriteLine(e.StackTrace);
            }
            return __s;
        }
        public List<string> DownloadremoteFTP(push_server push_server, string localFolder)
        {
            FtpWebRequest ftpRequest = null;
            FtpWebResponse ftpResponse = null;
            Stream ftpStream = null;
            int bufferSize = 2048;
            string url = push_server.ftp_ip + push_server.ftp_folder;
            urlFolder = new List<string>();
            urlFile = new List<string>();
            List<string> fileList = new List<string>();
            try
            {
                //==============================================================================
                List<string> _urlFolder = new List<string>();
                ftpRequest = (FtpWebRequest)FtpWebRequest.Create("ftp://test");
                ftpRequest.Credentials = new NetworkCredential(push_server.ftp_username, push_server.ftp_pwd);
                ftpRequest.UseBinary = true;
                ftpRequest.UsePassive = true;
                ftpRequest.KeepAlive = true;
                ftpRequest.Method = WebRequestMethods.Ftp.ListDirectoryDetails;
                ftpRequest.UsePassive = false;
                StreamReader ftpReader = null;
                ///
                ListFtpDirectory(url, ftpRequest, ftpResponse, ftpStream, ftpReader, push_server);
                ///
                foreach (var item in urlFolder)
                {
                    var result = urlFolder.Where(x => x.Contains(item));
                    if (result.Count() == 1)
                    {
                        _urlFolder.Add(item);
                    }
                }
                //==============================================================================
                foreach (var _url in _urlFolder)
                {
                    ftpRequest = (FtpWebRequest)FtpWebRequest.Create(_url);

                    List<string> tempUrl = new List<string>();
                    ftpRequest.Credentials = new NetworkCredential(push_server.ftp_username, push_server.ftp_pwd);
                    ftpRequest.Method = WebRequestMethods.Ftp.ListDirectoryDetails;
                    ftpResponse = (FtpWebResponse)ftpRequest.GetResponse();
                    ftpStream = ftpResponse.GetResponseStream();
                    ftpReader = new StreamReader(ftpStream);

                    while (!ftpReader.EndOfStream)
                    {
                        string line = ftpReader.ReadLine();
                        if (line.Trim().ToLower().StartsWith("d") || line.Contains(" <DIR> "))
                        {
                        }
                        else
                        {
                            Regex regex = new Regex(pattern);
                            IFormatProvider culture = CultureInfo.GetCultureInfo("en-us");
                            Match match = regex.Match(line);
                            string fileName = match.Groups[3].Value;
                            DateTime LastModified = DateTime.ParseExact(match.Groups[1].Value, "MM-dd-yy  hh:mmtt", System.Globalization.CultureInfo.InvariantCulture);
                            string fileUrl = _url + fileName;
                            int i = DateTime.Compare(push_server.ftp_lasted_manual, LastModified);

                            if (i > 0)
                            {

                            }
                            else //lasted som hom resp.LastModified
                            {
                                urlFile.Add(fileUrl);
                            }
                        }
                    }
                }
                //==============================================================================
                //List<string> fileList = new List<string>();
                foreach (var fileUri in urlFile)
                {
                    string formatString = "yyyyMMddHHmmss";
                    DateTime createdFile = DateTime.ParseExact(fileUri.Substring(fileUri.Length - 18, 14), formatString, null);
                    int result = DateTime.Compare(createdFile, push_server.ftp_lasted);
                    if (
                        result >= 0
                        )
                    {
                        Char delimiter = '/';
                        string[] fileInfo = fileUri.Split(delimiter);

                        ftpRequest = (FtpWebRequest)FtpWebRequest.Create(fileUri);
                        ftpRequest.Credentials = new NetworkCredential(push_server.ftp_username, push_server.ftp_pwd);
                        ftpRequest.Method = WebRequestMethods.Ftp.DownloadFile;
                        ftpResponse = (FtpWebResponse)ftpRequest.GetResponse();
                        ftpStream = ftpResponse.GetResponseStream();
                        FileStream localFileStream = new FileStream(localFolder + "\\" + fileInfo[fileInfo.Count() - 1], FileMode.Create);
                        byte[] byteBuffer = new byte[bufferSize];
                        int bytesRead = ftpStream.Read(byteBuffer, 0, bufferSize);

                        while (bytesRead > 0)
                        {
                            localFileStream.Write(byteBuffer, 0, bytesRead);
                            bytesRead = ftpStream.Read(byteBuffer, 0, bufferSize);
                        }
                        localFileStream.Close();

                        fileList.Add(localFolder + "\\" + fileInfo[fileInfo.Count() - 1]);
                    }
                }
                //==============================================================================
                /* Resource Cleanup */
                ftpReader.Close();
                ftpStream.Close();
                ftpResponse.Close();
                ftpRequest = null;
                //=================================================================================
                return fileList;

            } catch (Exception e)
            {
                Console.WriteLine(e.StackTrace);
                Console.WriteLine(e.Message);
                return fileList;
            }
        }
        public static void ListFtpDirectory(string url, FtpWebRequest ftpRequest, FtpWebResponse ftpResponse, Stream ftpStream, StreamReader ftpReader, push_server push_server)
        {
            /* Create an FTP Request */
            ftpRequest = (FtpWebRequest)FtpWebRequest.Create(url);
            ftpRequest.Credentials = new NetworkCredential(push_server.ftp_username, push_server.ftp_pwd);
            ftpRequest.Method = WebRequestMethods.Ftp.ListDirectoryDetails;
            ftpResponse = (FtpWebResponse)ftpRequest.GetResponse();
            ftpStream = ftpResponse.GetResponseStream();
            ftpReader = new StreamReader(ftpStream);

            List<string> tempUrl = new List<string>();

            while (!ftpReader.EndOfStream)
            {
                string line = ftpReader.ReadLine();
                if (line.Trim().ToLower().StartsWith("d") || line.Contains(" <DIR> "))
                {
                    Regex regex = new Regex(pattern);
                    IFormatProvider culture = CultureInfo.GetCultureInfo("en-us");
                    Match match = regex.Match(line);
                    string fileName = match.Groups[3].Value;
                    string fileUrl = url + fileName;

                    tempUrl.Add(fileUrl);
                }
            }
            

            foreach (string line in tempUrl)
            {
                urlFolder.Add(line + "/");
                ListFtpDirectory(line + "/", ftpRequest, ftpResponse, ftpStream, ftpReader,push_server);
            }
            //return urlFolder;
        }
        public List<string> GetLocalFTP_Auto(push_server push_server, string localFolder)
        {
            string localFTPFolder = push_server.ftp_folder;
            localFolder = localFolder + "\\";
            List<string> __s = null;
            try
            {
                DirectoryInfo d = new DirectoryInfo(localFTPFolder); //Assuming Test is your Folder
                FileInfo[] Files = d.GetFiles("*.txt", SearchOption.AllDirectories); //Getting Text files

                //foreach (FileInfo file in Files)
                //{
                //    if (file.CreationTime.CompareTo(push_server.ftp_lasted) > 0) {
                //        s.Add(file.FullName);
                //    }
                //}

                var _s = Files.Select(y => new { y.FullName, y.Name , y.CreationTime })
                    .Where(x => x.CreationTime.CompareTo(DateTime.Now.AddDays(-1)) > 0)
                    .ToList();

                __s = _s.Select(y => localFolder + y.Name).ToList();

                foreach (var ele in _s)
                {
                    var index = _s.IndexOf(ele);
                    System.IO.File.Copy(ele.FullName, __s.ElementAt(index), true);
                }
            }
            catch (Exception e)
            {

                Console.WriteLine(e.StackTrace);
            }
            return __s;
        }
        public List<string> DownloadremoteFTP_Auto(push_server push_server, string localFolder)
        {
            FtpWebRequest ftpRequest = null;
            FtpWebResponse ftpResponse = null;
            Stream ftpStream = null;
            int bufferSize = 2048;
            string url = push_server.ftp_ip + push_server.ftp_folder;
            urlFolder = new List<string>();
            urlFile = new List<string>();
            List<string> fileList = new List<string>();
            try
            {
                //==============================================================================
                string now = DateTime.Now.ToString("yyyy/MM/dd");
                string before_now = DateTime.Now.AddDays(-1).ToString("yyyy/MM/dd");
                List<string> _urlFolder = new List<string>();
                _urlFolder.Add(url + now);
                _urlFolder.Add(url + before_now);

                ftpRequest = (FtpWebRequest)FtpWebRequest.Create("ftp://test");
                ftpRequest.Credentials = new NetworkCredential(push_server.ftp_username, push_server.ftp_pwd);
                ftpRequest.UseBinary = true;
                ftpRequest.UsePassive = true;
                ftpRequest.KeepAlive = true;
                ftpRequest.Method = WebRequestMethods.Ftp.ListDirectoryDetails;
                ftpRequest.UsePassive = false;
                StreamReader ftpReader = null;
                /////
                //ListFtpDirectory(url, ftpRequest, ftpResponse, ftpStream, ftpReader);
                /////
                //foreach (var item in urlFolder)
                //{
                //    var result = urlFolder.Where(x => x.Contains(item));
                //    if (result.Count() == 1)
                //    {
                //        _urlFolder.Add(item);
                //    }
                //}
                //==============================================================================
                foreach (var _url in _urlFolder)
                {
                    ftpRequest = (FtpWebRequest)FtpWebRequest.Create(_url);

                    List<string> tempUrl = new List<string>();
                    ftpRequest.Credentials = new NetworkCredential(push_server.ftp_username, push_server.ftp_pwd);
                    ftpRequest.Method = WebRequestMethods.Ftp.ListDirectoryDetails;
                    ftpResponse = (FtpWebResponse)ftpRequest.GetResponse();
                    ftpStream = ftpResponse.GetResponseStream();
                    ftpReader = new StreamReader(ftpStream);

                    while (!ftpReader.EndOfStream)
                    {
                        string line = ftpReader.ReadLine();
                        if (line.Trim().ToLower().StartsWith("d") || line.Contains(" <DIR> "))
                        {
                        }
                        else
                        {
                            Regex regex = new Regex(pattern);
                            IFormatProvider culture = CultureInfo.GetCultureInfo("en-us");
                            Match match = regex.Match(line);
                            string fileName = match.Groups[3].Value;
                            DateTime LastModified = DateTime.ParseExact(match.Groups[1].Value, "MM-dd-yy  hh:mmtt", System.Globalization.CultureInfo.InvariantCulture);
                            string fileUrl = _url + "/" + fileName;
                            int i = DateTime.Compare(push_server.ftp_lasted_manual, LastModified);

                            if (i > 0)
                            {

                            }
                            else //lasted som hom resp.LastModified
                            {
                                urlFile.Add(fileUrl);
                            }
                        }
                    }
                }
                //==============================================================================
                //List<string> fileList = new List<string>();
                foreach (var fileUri in urlFile)
                {
                    string formatString = "yyyyMMddHHmmss";
                    DateTime createdFile = DateTime.ParseExact(fileUri.Substring(fileUri.Length - 18, 14), formatString, null);
                    int result = DateTime.Compare(createdFile, push_server.ftp_lasted);
                    Char delimiter = '/';
                    string[] fileInfo = fileUri.Split(delimiter);

                    ftpRequest = (FtpWebRequest)FtpWebRequest.Create(fileUri);
                    ftpRequest.Credentials = new NetworkCredential(push_server.ftp_username, push_server.ftp_pwd);
                    ftpRequest.Method = WebRequestMethods.Ftp.DownloadFile;
                    ftpResponse = (FtpWebResponse)ftpRequest.GetResponse();
                    ftpStream = ftpResponse.GetResponseStream();
                    FileStream localFileStream = new FileStream(localFolder + "\\" + fileInfo[fileInfo.Count() - 1], FileMode.Create);
                    byte[] byteBuffer = new byte[bufferSize];
                    int bytesRead = ftpStream.Read(byteBuffer, 0, bufferSize);

                    while (bytesRead > 0)
                    {
                        localFileStream.Write(byteBuffer, 0, bytesRead);
                        bytesRead = ftpStream.Read(byteBuffer, 0, bufferSize);
                    }
                    localFileStream.Close();

                    fileList.Add(localFolder + "\\" + fileInfo[fileInfo.Count() - 1]);

                }
                //==============================================================================
                /* Resource Cleanup */
                ftpReader.Close();
                ftpStream.Close();
                ftpResponse.Close();
                ftpRequest = null;
                //=================================================================================
                return fileList;

            }
            catch (Exception e)
            {
                Console.WriteLine(e.StackTrace);
                Console.WriteLine(e.Message);
                return fileList;
            }
        }
        #endregion
        #region update data
        private double Calculator(double D, module mod)
        {
            double A;
            A = (double)((double)((double)(mod.output_min - mod.output_max) / (double)(mod.input_min - mod.input_max))
                * (double)(D - mod.input_min))
                + mod.output_min + mod.off_set;
            return A;
        }
        #endregion
        private void btnSetting_Click(object sender, EventArgs e)
        {
            if (GlobalVar.isLogin)
            {

            }
            else
            {
                frmLogin frm = new frmLogin(lang);
                frm.ShowDialog();
                if (!GlobalVar.isLogin)
                {
                    MessageBox.Show(lang.getText("login_before_to_do_this"));
                    return;
                }
            }
            frmConfiguration frmConfig = new frmConfiguration(lang, this);
            frmConfig.ShowDialog();
            initConfig();
        }
        private void frmNewMain_FormClosing(object sender, FormClosingEventArgs e)
        {
            try
            {
                is_close_form = true;
                data_value obj = calculateImmediately5Minute();
                data_value obj60min = calculateImmediately60Minute();
                Process.GetCurrentProcess().Kill();
                //MessageBox.Show("123");
                if (System.Windows.Forms.Application.MessageLoop)
                {
                    // WinForms app
                    System.Windows.Forms.Application.Exit();
                }
                else
                {
                    // Console app
                    System.Environment.Exit(Environment.ExitCode);
                }

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Process.GetCurrentProcess().Kill();
                //Environment.FailFast();
                //Application.Exit();
                //throw ex;
            }
        }
        private data_value calculateImmediately5Minute()
        {
            var obj = new data_5minute_value_repository().get_latest_info();
            return obj;
        }
        private data_value calculateImmediately60Minute()
        {
            var obj = new data_5minute_value_repository().get_latest_info();
            return obj;
        }
        private void btnMPS5Minute_Click(object sender, EventArgs e)
        {
            data_value obj = calculateImmediately5Minute();
            frm5MinuteMPS frm = new frm5MinuteMPS(obj, lang);
            frm.ShowDialog();
        }

        private void btnMPS1Hour_Click(object sender, EventArgs e)
        {
            data_value obj = calculateImmediately60Minute();
            frm1HourMPS frm = new frm1HourMPS(obj, lang);
            frm.ShowDialog();
        }
        private void btnMPSHistoryData_Click(object sender, EventArgs e)
        {
            frmHistoryMPS frm = new frmHistoryMPS(lang);
            frm.ShowDialog();
        }
        private void btnAllHistory_Click(object sender, EventArgs e)
        {
            frmHistoryAll frm = new frmHistoryAll(lang);
            frm.ShowDialog();
        }
        private void btnMaintenance_Click(object sender, EventArgs e)
        {
            if (GlobalVar.isLogin)
            {

            }
            else
            {
                frmLogin frm = new frmLogin(lang);
                frm.ShowDialog();
                if (!GlobalVar.isLogin)
                {
                    MessageBox.Show(lang.getText("login_before_to_do_this"));
                    return;
                }
            }
            frmMaintenance objMaintenance = new frmMaintenance(lang);
            //this.Hide();
            objMaintenance.ShowDialog();
            //this.Show();
        }
        private void btnUsers_Click(object sender, EventArgs e)
        {
            if (GlobalVar.isLogin)
            {

            }
            else
            {
                frmLogin frm = new frmLogin(lang);
                frm.ShowDialog();
            }
            if (GlobalVar.isAdmin())
            {
                frmUserManagement frmUM = new frmUserManagement(lang);
                frmUM.ShowDialog();
            }
            else
            {
                MessageBox.Show(lang.getText("right_permission_error"));
            }


        }
        private void btnLoginLogout_Click(object sender, EventArgs e)
        {
            if (GlobalVar.isLogin)
            {
                this.btnLoginLogout.BackgroundImage = global::DataLogger.Properties.Resources.logout;

                GlobalVar.isLogin = false;
                GlobalVar.loginUser = null;
            }
            else
            {
                this.btnLoginLogout.BackgroundImage = global::DataLogger.Properties.Resources.login;
                frmLogin frm = new frmLogin(lang);
                frm.ShowDialog();

                if (GlobalVar.isLogin)
                {
                    this.btnLoginLogout.BackgroundImage = global::DataLogger.Properties.Resources.logout;
                }
            }
        }
        private void settingForLoginStatus()
        {
            if (GlobalVar.isLogin)
            {
                this.btnLoginLogout.BackgroundImage = global::DataLogger.Properties.Resources.logout;
                setTextHeadingLogin("" + lang.getText("main_menu_welcome") + ", " + GlobalVar.loginUser.user_name + " !");
            }
            else
            {
                this.btnLoginLogout.BackgroundImage = global::DataLogger.Properties.Resources.login;
                setTextHeadingLogin("" + lang.getText("main_menu_welcome") + ", " + lang.getText("main_menu_guest") + " !");
            }
        }
        private void btnMonthlyReport_Click(object sender, EventArgs e)
        {
            //if (MessageBox.Show(lang.getText("monthly_report_yesno_question"), lang.getText("confirm"), MessageBoxButtons.YesNo) == System.Windows.Forms.DialogResult.Yes)
            //{
            //    btnMonthlyReport.Enabled = false;
            //    vprgMonthlyReport.Value = 0;
            //    vprgMonthlyReport.Visible = true;

            //    bgwMonthlyReport.RunWorkerAsync();

            //    //Console.Write("1");
            //}
            Export f = new Export();
            var result = f.ShowDialog();
            if (result == DialogResult.Yes)
            {
                //get selected date
                dtpDateFrom = f.MydtpDateFrom;
                dtpDateTo = f.MydtpDateTo;
                btnMonthlyReport.Enabled = false;
                vprgMonthlyReport.Value = 0;
                vprgMonthlyReport.Visible = true;

                bgwMonthlyReport.RunWorkerAsync();
            }
        }
        private void btnLanguage_Click(object sender, EventArgs e)
        {
            switch_language();
            initConfig();
        }


        #region backgroundWorkerMonthlyReport
        private void backgroundWorkerMonthlyReport_DoWork(object sender, DoWorkEventArgs e)
        {
            //string appPath = Path.GetDirectoryName(Application.ExecutablePath);
            //string dataFolderName = "data";

            //string tempFileName = "monthly_report_template.xlsx";
            //string newFileName = "MonthlyReport_" + DateTime.Now.ToString("yyyy (MMddHHmmssfff)");

            //string tempFilePath = Path.Combine(appPath, dataFolderName, tempFileName);
            //string newFilePath = Path.Combine(appPath, dataFolderName, newFileName);

            //if (File.Exists(tempFilePath))
            //{
            //    int year = DateTime.Now.Year;
            //    double dayOfYearTotal = (new DateTime(year, 12, 31)).DayOfYear;
            //    double dayOfYear = 0;
            //    int percent = 0;

            //    IEnumerable<data_value> allData = db60m.get_all_for_monthly_report(year);

            //    if (allData != null)
            //    {
            //        Excel.XLWorkbook oExcelWorkbook = new Excel.XLWorkbook(tempFilePath);
            //        // Excel.Application oExcelApp = new Excel.Application();
            //        // Excel.Workbook oExcelWorkbook = oExcelApp.Workbooks.Open(tempFilePath, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value);

            //        const int startRow = 5;
            //        int row;

            //        List<MonthlyReportInfo> mps_ph = new List<MonthlyReportInfo>();
            //        List<MonthlyReportInfo> mps_orp = new List<MonthlyReportInfo>();
            //        List<MonthlyReportInfo> mps_do = new List<MonthlyReportInfo>();
            //        List<MonthlyReportInfo> mps_turbidity = new List<MonthlyReportInfo>();
            //        List<MonthlyReportInfo> mps_ec = new List<MonthlyReportInfo>();
            //        List<MonthlyReportInfo> mps_temp = new List<MonthlyReportInfo>();
            //        List<MonthlyReportInfo> tn = new List<MonthlyReportInfo>();
            //        List<MonthlyReportInfo> tp = new List<MonthlyReportInfo>();
            //        List<MonthlyReportInfo> toc = new List<MonthlyReportInfo>();
            //        List<MonthlyReportInfo> refrigeration_temperature = new List<MonthlyReportInfo>();
            //        List<MonthlyReportInfo> bottle_position = new List<MonthlyReportInfo>();

            //        for (int month = 1; month <= 12; month++)
            //        {
            //            Excel.IXLWorksheet oExcelWorksheet = oExcelWorkbook.Worksheet(month) as Excel.IXLWorksheet;
            //            // Excel.IXLWorkSheet oExcelWorksheet = oExcelWorkbook.Worksheets[month] as Excel.Worksheet;

            //            //rename the Sheet name
            //            oExcelWorksheet.Name = (new DateTime(year, month, 1)).ToString("MMM-yy");
            //            oExcelWorksheet.Cell(2, 1).Value = "'" + (new DateTime(year, month, 1)).ToString("MM.");
            //            oExcelWorksheet.Cell(2, 17).Value = (new DateTime(year, month, 1)).ToString("MMM-yy");

            //            // calculate average value
            //            for (int day = 1; day <= DateTime.DaysInMonth(year, month); day++)
            //            {
            //                // get maintenance by date (year, month, day)
            //                string strDate = year + "-" + month + "-" + day;
            //                IEnumerable<maintenance_log> onDateMaintenanceLogs = _maintenance_logs.get_all_by_date(strDate);
            //                // prepare data for maintenance
            //                string maintenance_operator_name = "";
            //                string maintenance_start_time = "";
            //                string maintenance_end_time = "";
            //                string maintenance_equipments = "";

            //                Color maintenance_color = StatusColorInfo.COL_STATUS_MAINTENANCE_PERIODIC;
            //                if (onDateMaintenanceLogs != null && onDateMaintenanceLogs.Count() > 0)
            //                {
            //                    foreach (maintenance_log itemMaintenanceLog in onDateMaintenanceLogs)
            //                    {
            //                        maintenance_operator_name += itemMaintenanceLog.name + ";";
            //                        maintenance_start_time += itemMaintenanceLog.start_time.ToString("HH")
            //                                                    + "h" + itemMaintenanceLog.start_time.ToString("mm") + ";";
            //                        maintenance_end_time += itemMaintenanceLog.end_time.ToString("HH")
            //                                                    + "h" + itemMaintenanceLog.end_time.ToString("mm") + ";";
            //                        if (itemMaintenanceLog.tn == 1)
            //                        {
            //                            maintenance_equipments += "TN;";
            //                        }
            //                        if (itemMaintenanceLog.tp == 1)
            //                        {
            //                            maintenance_equipments += "TP;";
            //                        }
            //                        if (itemMaintenanceLog.toc == 1)
            //                        {
            //                            maintenance_equipments += "TOC;";
            //                        }
            //                        if (itemMaintenanceLog.mps == 1)
            //                        {
            //                            maintenance_equipments += "MPS;";
            //                        }
            //                        if (itemMaintenanceLog.pumping_system == 1)
            //                        {
            //                            maintenance_equipments += "Pumping;";
            //                        }
            //                        if (itemMaintenanceLog.auto_sampler == 1)
            //                        {
            //                            maintenance_equipments += "AutoSampler;";
            //                        }
            //                        if (itemMaintenanceLog.other == 1)
            //                        {
            //                            maintenance_equipments += itemMaintenanceLog.other_para + ";";
            //                        }
            //                        if (itemMaintenanceLog.maintenance_reason == 1)
            //                        {
            //                            maintenance_color = StatusColorInfo.COL_STATUS_MAINTENANCE_INCIDENT;
            //                        }
            //                    }
            //                    maintenance_operator_name = maintenance_operator_name.Substring(0, maintenance_operator_name.Length - 1);
            //                    maintenance_start_time = maintenance_start_time.Substring(0, maintenance_start_time.Length - 1);
            //                    maintenance_end_time = maintenance_end_time.Substring(0, maintenance_end_time.Length - 1);
            //                    try
            //                    {
            //                        maintenance_equipments = maintenance_equipments.Substring(0, maintenance_equipments.Length - 1);
            //                    }
            //                    catch { }
            //                }

            //                IEnumerable<data_value> dayData = allData.Where(t => t.stored_date.Month == month && t.stored_date.Day == day);
            //                mps_ph.Clear();
            //                mps_orp.Clear();
            //                mps_do.Clear();
            //                mps_turbidity.Clear();
            //                mps_ec.Clear();
            //                mps_temp.Clear();
            //                tn.Clear();
            //                tp.Clear();
            //                toc.Clear();
            //                refrigeration_temperature.Clear();
            //                bottle_position.Clear();
            //                foreach (data_value item in dayData)
            //                {
            //                    mps_ph.AddNewDataValue(item.MPS_pH_status, item.MPS_pH);
            //                    mps_orp.AddNewDataValue(item.MPS_ORP_status, item.MPS_ORP);
            //                    mps_do.AddNewDataValue(item.MPS_DO_status, item.MPS_DO);
            //                    mps_turbidity.AddNewDataValue(item.MPS_Turbidity_status, item.MPS_Turbidity);
            //                    mps_ec.AddNewDataValue(item.MPS_EC_status, item.MPS_EC);
            //                    mps_temp.AddNewDataValue(item.MPS_Temp_status, item.MPS_Temp);
            //                    tn.AddNewDataValue(item.TN_status, item.TN);
            //                    tp.AddNewDataValue(item.TP_status, item.TP);
            //                    toc.AddNewDataValue(item.TOC_status, item.TOC);
            //                    refrigeration_temperature.AddNewDataValue(0, item.refrigeration_temperature);
            //                    bottle_position.AddNewDataValue(0, item.bottle_position);
            //                }

            //                // update to excel worksheet
            //                row = startRow + day;

            //                oExcelWorksheet.Cell(row, 2).Value = mps_ph.GetAverageOfMaxCountAsString();
            //                oExcelWorksheet.Cell(row, 3).Value = mps_orp.GetAverageOfMaxCountAsString();
            //                oExcelWorksheet.Cell(row, 4).Value = mps_do.GetAverageOfMaxCountAsString();
            //                oExcelWorksheet.Cell(row, 5).Value = mps_turbidity.GetAverageOfMaxCountAsString();
            //                oExcelWorksheet.Cell(row, 6).Value = mps_ec.GetAverageOfMaxCountAsString();
            //                oExcelWorksheet.Cell(row, 7).Value = mps_temp.GetAverageOfMaxCountAsString();
            //                oExcelWorksheet.Cell(row, 8).Value = tn.GetAverageOfMaxCountAsString();
            //                oExcelWorksheet.Cell(row, 9).Value = tp.GetAverageOfMaxCountAsString();
            //                oExcelWorksheet.Cell(row, 10).Value = toc.GetAverageOfMaxCountAsString();
            //                oExcelWorksheet.Cell(row, 11).Value = refrigeration_temperature.GetAverageOfMaxCountAsString();
            //                oExcelWorksheet.Cell(row, 12).Value = bottle_position.GetAverageOfMaxCountAsString();
            //                oExcelWorksheet.Cell(row, 14).Value = maintenance_operator_name;
            //                oExcelWorksheet.Cell(row, 15).Value = maintenance_start_time;
            //                oExcelWorksheet.Cell(row, 16).Value = maintenance_end_time;
            //                oExcelWorksheet.Cell(row, 17).Value = maintenance_equipments;


            //                oExcelWorksheet.Range("b" + row).Style.Fill.SetBackgroundColor(Excel.XLColor.FromColor(mps_ph.GetStatusColor()));
            //                oExcelWorksheet.Range("c" + row).Style.Fill.SetBackgroundColor(Excel.XLColor.FromColor(mps_orp.GetStatusColor()));
            //                oExcelWorksheet.Range("d" + row).Style.Fill.SetBackgroundColor(Excel.XLColor.FromColor(mps_do.GetStatusColor()));
            //                oExcelWorksheet.Range("e" + row).Style.Fill.SetBackgroundColor(Excel.XLColor.FromColor(mps_turbidity.GetStatusColor()));
            //                oExcelWorksheet.Range("f" + row).Style.Fill.SetBackgroundColor(Excel.XLColor.FromColor(mps_ec.GetStatusColor()));
            //                oExcelWorksheet.Range("g" + row).Style.Fill.SetBackgroundColor(Excel.XLColor.FromColor(mps_temp.GetStatusColor()));
            //                oExcelWorksheet.Range("h" + row).Style.Fill.SetBackgroundColor(Excel.XLColor.FromColor(tn.GetStatusColor()));
            //                oExcelWorksheet.Range("i" + row).Style.Fill.SetBackgroundColor(Excel.XLColor.FromColor(tp.GetStatusColor()));
            //                oExcelWorksheet.Range("j" + row).Style.Fill.SetBackgroundColor(Excel.XLColor.FromColor(toc.GetStatusColor()));
            //                oExcelWorksheet.Range("k" + row).Style.Fill.SetBackgroundColor(Excel.XLColor.FromColor(refrigeration_temperature.GetStatusColor()));
            //                oExcelWorksheet.Range("l" + row).Style.Fill.SetBackgroundColor(Excel.XLColor.FromColor(bottle_position.GetStatusColor()));

            //                oExcelWorksheet.Range("n" + row).Style.Fill.SetBackgroundColor(Excel.XLColor.FromColor(maintenance_color));
            //                oExcelWorksheet.Range("o" + row).Style.Fill.SetBackgroundColor(Excel.XLColor.FromColor(maintenance_color));
            //                oExcelWorksheet.Range("p" + row).Style.Fill.SetBackgroundColor(Excel.XLColor.FromColor(maintenance_color));
            //                oExcelWorksheet.Range("q" + row).Style.Fill.SetBackgroundColor(Excel.XLColor.FromColor(maintenance_color));

            //                dayOfYear = (new DateTime(year, month, day)).DayOfYear;
            //                percent = (int)(dayOfYear * 100d / dayOfYearTotal);
            //                bgwMonthlyReport.ReportProgress(percent);

            //                //Thread.Sleep(1);
            //            }
            //        }
            //        oExcelWorkbook.SaveAs(newFilePath + ".xlsx");
            //        //oExcelWorkbook.SaveAs(newFilePath, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Excel.XlSaveAsAccessMode.xlShared, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value);
            //    }
            //}
            //FileInfo fi = new FileInfo(newFilePath + ".xlsx");
            //if (fi.Exists)
            //{
            //    System.Diagnostics.Process.Start(newFilePath + ".xlsx");
            //}
            //else
            //{
            //    //file doesn't exist
            //}
        }
        private void backgroundWorkerMonthlyReport_DoWork_2(object sender, DoWorkEventArgs e)
        {
            Console.WriteLine("1");
            try
            {
                string appPath = Path.GetDirectoryName(Application.ExecutablePath);
                string dataFolderName = "data";

                //string tempFileName = "monthly_report_template.xlsx";
                string newFileName = "Report_" + DateTime.Now.ToString("HHmmssddMMyyyy") + ".xlsx";

                //string tempFilePath = Path.Combine(appPath, dataFolderName, tempFileName);
                string newFilePath = Path.Combine(appPath, dataFolderName, newFileName);

                DataTable dt_source = null;

                dt_source = db5m.get_all_mps(dtpDateFrom, dtpDateTo);

                if (
                    //File.Exists(tempFilePath)
                    true
                    )
                {
                    DataTable dt_view = new DataTable();

                    //dt_view.Columns.AddRange(new DataColumn[7]
                    //{
                    //new DataColumn("DateTime", typeof(string)),
                    //new DataColumn("COD", typeof(string)),
                    //new DataColumn("Color",typeof(string)),
                    //new DataColumn("pH", typeof(string)),
                    //new DataColumn("Temp", typeof(string)),
                    //new DataColumn("TSS", typeof(string)),
                    //new DataColumn("Flow", typeof(string))
                    //});

                    //dt_view.Columns.Add("DateTime");
                    //dt_view.Columns.Add("COD");
                    //dt_view.Columns.Add("Color");
                    //dt_view.Columns.Add("pH");
                    //dt_view.Columns.Add("Temp");
                    //dt_view.Columns.Add("TSS");
                    //dt_view.Columns.Add("Flow");

                    dt_view.Columns.Add("DateTime");
                    var _export = GlobalVar.moduleSettings.Where(m => m.type_value == 1);
                    foreach (var item in _export)
                    {
                        string currentvar = item.display_name;
                        dt_view.Columns.Add(currentvar);
                    }

                    DataRow viewrow = null;
                    if (dt_source == null)
                    {
                        return;
                    }
                    foreach (DataRow row in dt_source.Rows)
                    {
                        //countNow++;
                        viewrow = dt_view.NewRow();
                        //viewrow["DateTime"] = (Convert.ToDateTime(row["created"].ToString())).ToString("dd/MM/yyyy");
                        viewrow["DateTime"] = row["created"].ToString();
                        //viewrow["Time"] = ((int)row["stored_hour"]).ToString("00") + ":" + ((int)row["stored_minute"]).ToString("00") + ":00";

                        foreach (var item in _export)
                        {
                            string currentvar = item.item_name;
                            string currentvar_display = item.display_name;

                            if (Convert.ToDouble(row[currentvar]) == -1)
                            {
                                viewrow[currentvar_display] = "---";
                            }
                            else
                            {
                                if (Convert.ToDouble(row[currentvar]) > item.error_max || Convert.ToDouble(row[currentvar]) < item.error_min)
                                {
                                    viewrow[currentvar_display] = "---";
                                }
                                else
                                {
                                    viewrow[currentvar_display] = row[currentvar].ToString();
                                }
                            }
                        }

                        dt_view.Rows.Add(viewrow);

                    }
                    ////Exporting to Excel
                    //var wb = new XLWorkbook();
                    ////Add a DataTable as a worksheet
                    //wb.Worksheets.Add(dt_view);

                    //wb.SaveAs(newFilePath);
                    ExportToExcel(bgwMonthlyReport, dt_view, newFilePath);
                }

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.StackTrace);
            }
            //////////////////////////
        }
        // Export DataTable into an excel file with field names in the header line
        // - Save excel file without ever making it visible if filepath is given
        // - Don't save excel file, just make it visible if no filepath is given
        public static void ExportToExcel(BackgroundWorker b, DataTable tbl, string excelFilePath)
        {
            try
            {
                if (tbl == null || tbl.Columns.Count == 0)
                    throw new Exception("ExportToExcel: Null or empty input table!\n");
                // load excel, and create a new workbook
                Excel.Application oExcelApp = new Excel.Application();
                //excelApp.Workbooks.Add();
                //Excel.Workbook oExcelWorkbook = oExcelApp.Workbooks.Open(patch, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value);
                oExcelApp.Workbooks.Add();
                // single worksheet
                Excel._Worksheet workSheet = (Excel.Worksheet)oExcelApp.ActiveSheet;
                int percent = 0;
                int countNow = 0;
                // column headings
                for (var i = 0; i < tbl.Columns.Count; i++)
                {
                    workSheet.Cells[1, i + 1] = tbl.Columns[i].ColumnName;
                }

                // rows
                for (var i = 0; i < tbl.Rows.Count; i++)
                {
                    // to do: format datetime values before printing
                    for (var j = 0; j < tbl.Columns.Count; j++)
                    {
                        workSheet.Cells[i + 2, j + 1] = tbl.Rows[i][j];
                    }
                    countNow++;
                    percent = (int)(countNow * 100d / tbl.Rows.Count);
                    b.ReportProgress(percent);
                }

                // check file path
                if (!string.IsNullOrEmpty(excelFilePath))
                {
                    try
                    {
                        workSheet.SaveAs(excelFilePath);
                        oExcelApp.Quit();
                        MessageBox.Show("Excel file saved!", "", MessageBoxButtons.OK, MessageBoxIcon.None, MessageBoxDefaultButton.Button1, MessageBoxOptions.ServiceNotification);
                        //MessageBox.Show("Excel file saved!");
                    }
                    catch (Exception ex)
                    {
                        throw new Exception("ExportToExcel: Excel file could not be saved! Check filepath.\n"
                                            + ex.Message);
                    }
                }
                else
                { // no file path is given
                    oExcelApp.Visible = true;
                }
            }
            catch (Exception ex)
            {
                throw new Exception("ExportToExcel: \n" + ex.Message);
            }
        }
        private void backgroundWorkerMonthlyReport_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            vprgMonthlyReport.Value = e.ProgressPercentage;
        }

        private void backgroundWorkerMonthlyReport_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            btnMonthlyReport.Enabled = true;
            vprgMonthlyReport.Visible = false;

            if (!e.Cancelled && e.Error == null)
            {
                MessageBox.Show(lang.getText("successfully"), "", MessageBoxButtons.OK, MessageBoxIcon.None, MessageBoxDefaultButton.Button1, MessageBoxOptions.ServiceNotification);
                //MessageBox.Show(lang.getText("successfully"));
            }
            else
            {

            }
        }

        #endregion backgroundWorkerMonthlyReport

        private void txtvar2_Click(object sender, EventArgs e)
        {

        }

        private void panel14_Paint(object sender, PaintEventArgs e)
        {

        }

        private void txtvar1_Paint(object sender, PaintEventArgs e)
        {
     //       while (txtvar1.Width < System.Windows.Forms.TextRenderer.MeasureText(txtvar1.Text,
     //new Font(txtvar1.Font.FontFamily, txtvar1.Font.Size, txtvar1.Font.Style)).Width)
     //       {
     //           txtvar1.Font = new Font(txtvar1.Font.FontFamily, txtvar1.Font.Size - 0.5f, txtvar1.Font.Style);
     //       }
        }

        private void txtvar1Unit_Paint(object sender, PaintEventArgs e)
        {
//            while (txtvar1Unit.Width < System.Windows.Forms.TextRenderer.MeasureText(txtvar1Unit.Text,
//new Font(txtvar1Unit.Font.FontFamily, txtvar1Unit.Font.Size, txtvar1Unit.Font.Style)).Width)
//            {
//                txtvar1Unit.Font = new Font(txtvar1Unit.Font.FontFamily, txtvar1Unit.Font.Size - 0.5f, txtvar1Unit.Font.Style);
//            }
        }

        private void txtvar2_Paint(object sender, PaintEventArgs e)
        {
     //       while (txtvar2.Width < System.Windows.Forms.TextRenderer.MeasureText(txtvar2.Text,
     //new Font(txtvar2.Font.FontFamily, txtvar2.Font.Size, txtvar2.Font.Style)).Width)
     //       {
     //           txtvar2.Font = new Font(txtvar2.Font.FontFamily, txtvar2.Font.Size - 0.5f, txtvar2.Font.Style);
     //       }
        }

        private void txtvar2Unit_Paint(object sender, PaintEventArgs e)
        {
//            while (txtvar2Unit.Width < System.Windows.Forms.TextRenderer.MeasureText(txtvar2Unit.Text,
//new Font(txtvar2Unit.Font.FontFamily, txtvar2Unit.Font.Size, txtvar2Unit.Font.Style)).Width)
//            {
//                txtvar2Unit.Font = new Font(txtvar2Unit.Font.FontFamily, txtvar2Unit.Font.Size - 0.5f, txtvar2Unit.Font.Style);
//            }
        }

        private void txtvar3_Paint(object sender, PaintEventArgs e)
        {
//            while (txtvar3.Width < System.Windows.Forms.TextRenderer.MeasureText(txtvar3.Text,
//new Font(txtvar3.Font.FontFamily, txtvar3.Font.Size, txtvar3.Font.Style)).Width)
//            {
//                txtvar3.Font = new Font(txtvar3.Font.FontFamily, txtvar3.Font.Size - 0.5f, txtvar3.Font.Style);
//            }
        }

        private void txtvar3Unit_Paint(object sender, PaintEventArgs e)
        {
//            while (txtvar3Unit.Width < System.Windows.Forms.TextRenderer.MeasureText(txtvar3Unit.Text,
//new Font(txtvar3Unit.Font.FontFamily, txtvar3Unit.Font.Size, txtvar3Unit.Font.Style)).Width)
//            {
//                txtvar3Unit.Font = new Font(txtvar3Unit.Font.FontFamily, txtvar3Unit.Font.Size - 0.5f, txtvar3Unit.Font.Style);
//            }
        }

        private void txtvar4_Paint(object sender, PaintEventArgs e)
        {
//            while (txtvar4.Width < System.Windows.Forms.TextRenderer.MeasureText(txtvar4.Text,
//new Font(txtvar4.Font.FontFamily, txtvar4.Font.Size, txtvar4.Font.Style)).Width)
//            {
//                txtvar4.Font = new Font(txtvar4.Font.FontFamily, txtvar4.Font.Size - 0.5f, txtvar4.Font.Style);
//            }
        }

        private void txtvar4Unit_Paint(object sender, PaintEventArgs e)
        {
//            while (txtvar4Unit.Width < System.Windows.Forms.TextRenderer.MeasureText(txtvar4Unit.Text,
//new Font(txtvar4Unit.Font.FontFamily, txtvar4Unit.Font.Size, txtvar4Unit.Font.Style)).Width)
//            {
//                txtvar4Unit.Font = new Font(txtvar4Unit.Font.FontFamily, txtvar4Unit.Font.Size - 0.5f, txtvar4Unit.Font.Style);
//            }
        }

        private void txtvar5_Paint(object sender, PaintEventArgs e)
        {
//            while (txtvar5.Width < System.Windows.Forms.TextRenderer.MeasureText(txtvar5.Text,
//new Font(txtvar5.Font.FontFamily, txtvar5.Font.Size, txtvar5.Font.Style)).Width)
//            {
//                txtvar5.Font = new Font(txtvar5.Font.FontFamily, txtvar5.Font.Size - 0.5f, txtvar5.Font.Style);
//            }
        }

        private void txtvar5Unit_Paint(object sender, PaintEventArgs e)
        {
//            while (txtvar5Unit.Width < System.Windows.Forms.TextRenderer.MeasureText(txtvar5Unit.Text,
//new Font(txtvar5Unit.Font.FontFamily, txtvar5Unit.Font.Size, txtvar5Unit.Font.Style)).Width)
//            {
//                txtvar5Unit.Font = new Font(txtvar5Unit.Font.FontFamily, txtvar5Unit.Font.Size - 0.5f, txtvar5Unit.Font.Style);
//            }
        }

        private void txtvar6_Paint(object sender, PaintEventArgs e)
        {
//            while (txtvar6.Width < System.Windows.Forms.TextRenderer.MeasureText(txtvar6.Text,
//new Font(txtvar6.Font.FontFamily, txtvar6.Font.Size, txtvar6.Font.Style)).Width)
//            {
//                txtvar6.Font = new Font(txtvar6.Font.FontFamily, txtvar6.Font.Size - 0.5f, txtvar6.Font.Style);
//            }
        }

        private void txtvar6Unit_Paint(object sender, PaintEventArgs e)
        {
//            while (txtvar6Unit.Width < System.Windows.Forms.TextRenderer.MeasureText(txtvar6Unit.Text,
//new Font(txtvar6Unit.Font.FontFamily, txtvar6Unit.Font.Size, txtvar6Unit.Font.Style)).Width)
//            {
//                txtvar6Unit.Font = new Font(txtvar6Unit.Font.FontFamily, txtvar6Unit.Font.Size - 0.5f, txtvar6Unit.Font.Style);
//            }
        }

        private void txtvar7_Paint(object sender, PaintEventArgs e)
        {
//            while (txtvar7.Width < System.Windows.Forms.TextRenderer.MeasureText(txtvar7.Text,
//new Font(txtvar7.Font.FontFamily, txtvar7.Font.Size, txtvar7.Font.Style)).Width)
//            {
//                txtvar7.Font = new Font(txtvar7.Font.FontFamily, txtvar7.Font.Size - 0.5f, txtvar7.Font.Style);
//            }
        }

        private void txtvar7Unit_Paint(object sender, PaintEventArgs e)
        {
//            while (txtvar7Unit.Width < System.Windows.Forms.TextRenderer.MeasureText(txtvar7Unit.Text,
//new Font(txtvar7Unit.Font.FontFamily, txtvar7Unit.Font.Size, txtvar7Unit.Font.Style)).Width)
//            {
//                txtvar7Unit.Font = new Font(txtvar7Unit.Font.FontFamily, txtvar7Unit.Font.Size - 0.5f, txtvar7Unit.Font.Style);
//            }
        }

        private void txtvar8_Paint(object sender, PaintEventArgs e)
        {
//            while (txtvar8.Width < System.Windows.Forms.TextRenderer.MeasureText(txtvar8.Text,
//new Font(txtvar8.Font.FontFamily, txtvar8.Font.Size, txtvar8.Font.Style)).Width)
//            {
//                txtvar8.Font = new Font(txtvar8.Font.FontFamily, txtvar8.Font.Size - 0.5f, txtvar8.Font.Style);
//            }
        }

        private void txtvar8Unit_Paint(object sender, PaintEventArgs e)
        {
//            while (txtvar8Unit.Width < System.Windows.Forms.TextRenderer.MeasureText(txtvar8Unit.Text,
//new Font(txtvar8Unit.Font.FontFamily, txtvar8Unit.Font.Size, txtvar8Unit.Font.Style)).Width)
//            {
//                txtvar8Unit.Font = new Font(txtvar8Unit.Font.FontFamily, txtvar8Unit.Font.Size - 0.5f, txtvar8Unit.Font.Style);
//            }
        }

        private void txtvar9_Paint(object sender, PaintEventArgs e)
        {
//            while (txtvar9.Width < System.Windows.Forms.TextRenderer.MeasureText(txtvar9.Text,
//new Font(txtvar9.Font.FontFamily, txtvar9.Font.Size, txtvar9.Font.Style)).Width)
//            {
//                txtvar9.Font = new Font(txtvar9.Font.FontFamily, txtvar9.Font.Size - 0.5f, txtvar9.Font.Style);
//            }
        }

        private void txtvar9Unit_Paint(object sender, PaintEventArgs e)
        {
//            while (txtvar9Unit.Width < System.Windows.Forms.TextRenderer.MeasureText(txtvar9Unit.Text,
//new Font(txtvar9Unit.Font.FontFamily, txtvar9Unit.Font.Size, txtvar9Unit.Font.Style)).Width)
//            {
//                txtvar9Unit.Font = new Font(txtvar9Unit.Font.FontFamily, txtvar9Unit.Font.Size - 0.5f, txtvar9Unit.Font.Style);
//            }
        }

        private void txtvar10_Paint(object sender, PaintEventArgs e)
        {
//            while (txtvar10.Width < System.Windows.Forms.TextRenderer.MeasureText(txtvar10.Text,
//new Font(txtvar10.Font.FontFamily, txtvar10.Font.Size, txtvar10.Font.Style)).Width)
//            {
//                txtvar10.Font = new Font(txtvar10.Font.FontFamily, txtvar10.Font.Size - 0.5f, txtvar10.Font.Style);
//            }
        }

        private void txtvar10Unit_Paint(object sender, PaintEventArgs e)
        {
//            while (txtvar10Unit.Width < System.Windows.Forms.TextRenderer.MeasureText(txtvar10Unit.Text,
//new Font(txtvar10Unit.Font.FontFamily, txtvar10Unit.Font.Size, txtvar10Unit.Font.Style)).Width)
//            {
//                txtvar10Unit.Font = new Font(txtvar10Unit.Font.FontFamily, txtvar10Unit.Font.Size - 0.5f, txtvar10Unit.Font.Style);
//            }
        }

        private void txtvar11_Paint(object sender, PaintEventArgs e)
        {
//            while (txtvar11.Width < System.Windows.Forms.TextRenderer.MeasureText(txtvar11.Text,
//new Font(txtvar11.Font.FontFamily, txtvar11.Font.Size, txtvar11.Font.Style)).Width)
//            {
//                txtvar11.Font = new Font(txtvar11.Font.FontFamily, txtvar11.Font.Size - 0.5f, txtvar11.Font.Style);
//            }
        }

        private void txtvar11Unit_Paint(object sender, PaintEventArgs e)
        {
//            while (txtvar11Unit.Width < System.Windows.Forms.TextRenderer.MeasureText(txtvar11Unit.Text,
//new Font(txtvar11Unit.Font.FontFamily, txtvar11Unit.Font.Size, txtvar11Unit.Font.Style)).Width)
//            {
//                txtvar11Unit.Font = new Font(txtvar11Unit.Font.FontFamily, txtvar11Unit.Font.Size - 0.5f, txtvar11Unit.Font.Style);
//            }
        }

        private void txtvar12_Paint(object sender, PaintEventArgs e)
        {
//            while (txtvar12.Width < System.Windows.Forms.TextRenderer.MeasureText(txtvar12.Text,
//new Font(txtvar12.Font.FontFamily, txtvar12.Font.Size, txtvar12.Font.Style)).Width)
//            {
//                txtvar12.Font = new Font(txtvar12.Font.FontFamily, txtvar12.Font.Size - 0.5f, txtvar12.Font.Style);
//            }
        }

        private void txtvar12Unit_Paint(object sender, PaintEventArgs e)
        {
//            while (txtvar12Unit.Width < System.Windows.Forms.TextRenderer.MeasureText(txtvar12Unit.Text,
//new Font(txtvar12Unit.Font.FontFamily, txtvar12Unit.Font.Size, txtvar12Unit.Font.Style)).Width)
//            {
//                txtvar12Unit.Font = new Font(txtvar12Unit.Font.FontFamily, txtvar12Unit.Font.Size - 0.5f, txtvar12Unit.Font.Style);
//            }
        }

        private void txtvar13_Paint(object sender, PaintEventArgs e)
        {
//            while (txtvar13.Width < System.Windows.Forms.TextRenderer.MeasureText(txtvar13.Text,
//new Font(txtvar13.Font.FontFamily, txtvar13.Font.Size, txtvar13.Font.Style)).Width)
//            {
//                txtvar13.Font = new Font(txtvar13.Font.FontFamily, txtvar13.Font.Size - 0.5f, txtvar13.Font.Style);
//            }
        }

        private void txtvar13Unit_Paint(object sender, PaintEventArgs e)
        {
//            while (txtvar13Unit.Width < System.Windows.Forms.TextRenderer.MeasureText(txtvar13Unit.Text,
//new Font(txtvar13Unit.Font.FontFamily, txtvar13Unit.Font.Size, txtvar13Unit.Font.Style)).Width)
//            {
//                txtvar13Unit.Font = new Font(txtvar13Unit.Font.FontFamily, txtvar13Unit.Font.Size - 0.5f, txtvar13Unit.Font.Style);
//            }
        }

        private void txtvar14_Paint(object sender, PaintEventArgs e)
        {
//            while (txtvar14.Width < System.Windows.Forms.TextRenderer.MeasureText(txtvar14.Text,
//new Font(txtvar14.Font.FontFamily, txtvar14.Font.Size, txtvar14.Font.Style)).Width)
//            {
//                txtvar14.Font = new Font(txtvar14.Font.FontFamily, txtvar14.Font.Size - 0.5f, txtvar14.Font.Style);
//            }
        }

        private void txtvar14Unit_Paint(object sender, PaintEventArgs e)
        {
//            while (txtvar14Unit.Width < System.Windows.Forms.TextRenderer.MeasureText(txtvar14Unit.Text,
//new Font(txtvar14Unit.Font.FontFamily, txtvar14Unit.Font.Size, txtvar14Unit.Font.Style)).Width)
//            {
//                txtvar14Unit.Font = new Font(txtvar14Unit.Font.FontFamily, txtvar14Unit.Font.Size - 0.5f, txtvar14Unit.Font.Style);
//            }
        }

        private void txtvar15_Paint(object sender, PaintEventArgs e)
        {
//            while (txtvar15.Width < System.Windows.Forms.TextRenderer.MeasureText(txtvar15.Text,
//new Font(txtvar15.Font.FontFamily, txtvar15.Font.Size, txtvar15.Font.Style)).Width)
//            {
//                txtvar15.Font = new Font(txtvar15.Font.FontFamily, txtvar15.Font.Size - 0.5f, txtvar15.Font.Style);
//            }
        }

        private void txtvar15Unit_Paint(object sender, PaintEventArgs e)
        {
//            while (txtvar15Unit.Width < System.Windows.Forms.TextRenderer.MeasureText(txtvar15Unit.Text,
//new Font(txtvar15Unit.Font.FontFamily, txtvar15Unit.Font.Size, txtvar15Unit.Font.Style)).Width)
//            {
//                txtvar15Unit.Font = new Font(txtvar15Unit.Font.FontFamily, txtvar15Unit.Font.Size - 0.5f, txtvar15Unit.Font.Style);
//            }
        }

        private void txtvar16_Paint(object sender, PaintEventArgs e)
        {
//            while (txtvar16.Width < System.Windows.Forms.TextRenderer.MeasureText(txtvar16.Text,
//new Font(txtvar16.Font.FontFamily, txtvar16.Font.Size, txtvar16.Font.Style)).Width)
//            {
//                txtvar16.Font = new Font(txtvar16.Font.FontFamily, txtvar16.Font.Size - 0.5f, txtvar16.Font.Style);
//            }
        }

        private void txtvar16Unit_Paint(object sender, PaintEventArgs e)
        {
//            while (txtvar16Unit.Width < System.Windows.Forms.TextRenderer.MeasureText(txtvar16Unit.Text,
//new Font(txtvar16Unit.Font.FontFamily, txtvar16Unit.Font.Size, txtvar16Unit.Font.Style)).Width)
//            {
//                txtvar16Unit.Font = new Font(txtvar16Unit.Font.FontFamily, txtvar16Unit.Font.Size - 0.5f, txtvar16Unit.Font.Style);
//            }
        }

        private void txtvar17_Paint(object sender, PaintEventArgs e)
        {
//            while (txtvar17.Width < System.Windows.Forms.TextRenderer.MeasureText(txtvar17.Text,
//new Font(txtvar17.Font.FontFamily, txtvar17.Font.Size, txtvar17.Font.Style)).Width)
//            {
//                txtvar17.Font = new Font(txtvar17.Font.FontFamily, txtvar17.Font.Size - 0.5f, txtvar17.Font.Style);
//            }
        }

        private void txtvar17Unit_Paint(object sender, PaintEventArgs e)
        {
//            while (txtvar17Unit.Width < System.Windows.Forms.TextRenderer.MeasureText(txtvar17Unit.Text,
//new Font(txtvar17Unit.Font.FontFamily, txtvar17Unit.Font.Size, txtvar17Unit.Font.Style)).Width)
//            {
//                txtvar17Unit.Font = new Font(txtvar17Unit.Font.FontFamily, txtvar17Unit.Font.Size - 0.5f, txtvar17Unit.Font.Style);
//            }
        }

        private void txtvar18_Paint(object sender, PaintEventArgs e)
        {
//            while (txtvar18.Width < System.Windows.Forms.TextRenderer.MeasureText(txtvar18.Text,
//new Font(txtvar18.Font.FontFamily, txtvar18.Font.Size, txtvar18.Font.Style)).Width)
//            {
//                txtvar18.Font = new Font(txtvar18.Font.FontFamily, txtvar18.Font.Size - 0.5f, txtvar18.Font.Style);
//            }
        }

        private void txtvar18Unit_Paint(object sender, PaintEventArgs e)
        {
//            while (txtvar18Unit.Width < System.Windows.Forms.TextRenderer.MeasureText(txtvar18Unit.Text,
//new Font(txtvar18Unit.Font.FontFamily, txtvar18Unit.Font.Size, txtvar18Unit.Font.Style)).Width)
//            {
//                txtvar18Unit.Font = new Font(txtvar18Unit.Font.FontFamily, txtvar18Unit.Font.Size - 0.5f, txtvar18Unit.Font.Style);
//            }
        }

        private void txtvar19_Paint(object sender, PaintEventArgs e)
        {
//            while (txtvar19.Width < System.Windows.Forms.TextRenderer.MeasureText(txtvar19.Text,
//new Font(txtvar19.Font.FontFamily, txtvar19.Font.Size, txtvar19.Font.Style)).Width)
//            {
//                txtvar19.Font = new Font(txtvar19.Font.FontFamily, txtvar19.Font.Size - 0.5f, txtvar19.Font.Style);
//            }
        }

        private void txtvar19Unit_Paint(object sender, PaintEventArgs e)
        {
//            while (txtvar19Unit.Width < System.Windows.Forms.TextRenderer.MeasureText(txtvar19Unit.Text,
//new Font(txtvar19Unit.Font.FontFamily, txtvar19Unit.Font.Size, txtvar19Unit.Font.Style)).Width)
//            {
//                txtvar19Unit.Font = new Font(txtvar19Unit.Font.FontFamily, txtvar19Unit.Font.Size - 0.5f, txtvar19Unit.Font.Style);
//            }
        }

        private void txtvar20_Paint(object sender, PaintEventArgs e)
        {
//            while (txtvar20.Width < System.Windows.Forms.TextRenderer.MeasureText(txtvar20.Text,
//new Font(txtvar20.Font.FontFamily, txtvar20.Font.Size, txtvar20.Font.Style)).Width)
//            {
//                txtvar20.Font = new Font(txtvar20.Font.FontFamily, txtvar20.Font.Size - 0.5f, txtvar20.Font.Style);
//            }
        }

        private void txtvar20Unit_Paint(object sender, PaintEventArgs e)
        {
//            while (txtvar20Unit.Width < System.Windows.Forms.TextRenderer.MeasureText(txtvar20Unit.Text,
//new Font(txtvar20Unit.Font.FontFamily, txtvar20Unit.Font.Size, txtvar20Unit.Font.Style)).Width)
//            {
//                txtvar20Unit.Font = new Font(txtvar20Unit.Font.FontFamily, txtvar20Unit.Font.Size - 0.5f, txtvar20Unit.Font.Style);
//            }
        }

        private void txtvar21_Paint(object sender, PaintEventArgs e)
        {
            while (txtvar21.Width < System.Windows.Forms.TextRenderer.MeasureText(txtvar21.Text,
new Font(txtvar21.Font.FontFamily, txtvar21.Font.Size, txtvar21.Font.Style)).Width)
            {
                txtvar21.Font = new Font(txtvar21.Font.FontFamily, txtvar21.Font.Size - 0.5f, txtvar21.Font.Style);
            }
        }

        private void txtvar21Unit_Paint(object sender, PaintEventArgs e)
        {
//            while (txtvar21Unit.Width < System.Windows.Forms.TextRenderer.MeasureText(txtvar21Unit.Text,
//new Font(txtvar21Unit.Font.FontFamily, txtvar21Unit.Font.Size, txtvar21Unit.Font.Style)).Width)
//            {
//                txtvar21Unit.Font = new Font(txtvar21Unit.Font.FontFamily, txtvar21Unit.Font.Size - 0.5f, txtvar21Unit.Font.Style);
//            }
        }

        private void txtvar22_Paint(object sender, PaintEventArgs e)
        {
//            while (txtvar22.Width < System.Windows.Forms.TextRenderer.MeasureText(txtvar22.Text,
//new Font(txtvar22.Font.FontFamily, txtvar22.Font.Size, txtvar22.Font.Style)).Width)
//            {
//                txtvar22.Font = new Font(txtvar22.Font.FontFamily, txtvar22.Font.Size - 0.5f, txtvar22.Font.Style);
//            }
        }

        private void txtvar22Unit_Paint(object sender, PaintEventArgs e)
        {
//            while (txtvar22Unit.Width < System.Windows.Forms.TextRenderer.MeasureText(txtvar22Unit.Text,
//new Font(txtvar22Unit.Font.FontFamily, txtvar22Unit.Font.Size, txtvar22Unit.Font.Style)).Width)
//            {
//                txtvar22Unit.Font = new Font(txtvar22Unit.Font.FontFamily, txtvar22Unit.Font.Size - 0.5f, txtvar22Unit.Font.Style);
//            }
        }

        private void txtvar10Unit_Click(object sender, EventArgs e)
        {

        }

        private void txtvar12Unit_Click(object sender, EventArgs e)
        {

        }

        private void label10_Click(object sender, EventArgs e)
        {

        }
    }
    public class ReceivedEventArgs : EventArgs
    {
        public byte[] Data { get; set; }
    }
}
