﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using Npgsql;
using DataLogger.Entities;

namespace DataLogger.Data
{
    public class module_data_binding_repository: NpgsqlDBConnection
    {     
        #region Public procedure

        public IEnumerable<databinding> get_all()
        {
            List<databinding> listUser = new List<databinding>();
            using (NpgsqlDBConnection db = new NpgsqlDBConnection())
            {
                try
                {   
                    if (db.open_connection())
                    {
                        string sql_command = "SELECT * FROM databinding";
                        using (NpgsqlCommand cmd = db._conn.CreateCommand())
                        {                            
                            cmd.CommandText = sql_command;
                            NpgsqlDataReader reader = cmd.ExecuteReader();
                            while (reader.Read())
                            {
                                databinding obj = new databinding();
                                obj = (databinding)_get_info(reader);
                                listUser.Add(obj);
                            }
                            reader.Close();
                            db.close_connection();
                            return listUser;
                        }
                    }
                    else
                    {
                        db.close_connection();
                        return null;
                    }
                }
                catch
                {
                    if (db != null)
                    {
                        db.close_connection();
                    }
                    return null;
                }
                finally
                { db.close_connection(); }
            }
        }

        /// <summary>
        /// get info by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public databinding get_info_by_id(int id)
        {
            using (NpgsqlDBConnection db = new NpgsqlDBConnection())
            {
                try
                {

                    databinding obj = null;
                    if (db.open_connection())
                    {
                        string sql_command = "SELECT * FROM databinding WHERE id = " + id;
                        sql_command += " LIMIT 1";
                        using (NpgsqlCommand cmd = db._conn.CreateCommand())
                        {                            
                            cmd.CommandText = sql_command;

                            NpgsqlDataReader reader = cmd.ExecuteReader();

                            while (reader.Read())
                            {
                                obj = new databinding();
                                obj = (databinding)_get_info(reader);
                                break;
                            }
                            reader.Close();
                            db.close_connection();
                            return obj;
                        }
                    }
                    else
                    {
                        db.close_connection();
                        return null;
                    }
                }
                catch
                {
                    if (db != null)
                    {
                        db.close_connection();
                    }
                    return null;
                }
                finally
                { db.close_connection(); }
            }
        }
        
        #endregion Public procedure

        #region private procedure

        private databinding _get_info(NpgsqlDataReader dataReader)
        {
            databinding obj = new databinding();
            try
            {
                if (!DBNull.Value.Equals(dataReader["id"]))
                    obj.id = Convert.ToInt32(dataReader["id"].ToString().Trim());
                else
                    obj.id = 0;

                if (!DBNull.Value.Equals(dataReader["min_value"]))
                    obj.min_value = Convert.ToInt32(dataReader["min_value"].ToString().Trim());
                else
                    obj.min_value = 0;

                if (!DBNull.Value.Equals(dataReader["code"]))
                    obj.code = dataReader["code"].ToString().Trim();
                else
                    obj.code = "";
                if (!DBNull.Value.Equals(dataReader["clnnamevalue"]))
                    obj.clnnamevalue = dataReader["clnnamevalue"].ToString().Trim();
                else
                    obj.clnnamevalue = "";
                if (!DBNull.Value.Equals(dataReader["clnnamestatus"]))
                    obj.clnnamestatus = dataReader["clnnamestatus"].ToString().Trim();
                else
                    obj.clnnamestatus = "";
            }
            catch (Exception ex)
            {                
                throw ex;             
            }
            return obj;
        }

        #endregion private procedure
    }
}