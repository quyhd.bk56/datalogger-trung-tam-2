﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLogger.Entities
{
    public class databinding
    {
        public int id { get; set; }
        public int min_value { get; set; }
        public string code { get; set; }
        public string clnnamevalue { get; set; }
        public string clnnamestatus { get; set; }


        public databinding()
        {
            id = -1;
            min_value = 0;
            code = "";
            clnnamevalue = "";
            clnnamestatus = "";
        }
    }
}
